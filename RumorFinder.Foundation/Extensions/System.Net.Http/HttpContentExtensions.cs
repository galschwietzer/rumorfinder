﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RumorFinder.Foundation.Extensions.System.Net.Http
{

    internal static class HttpContentExtensions
    {

        /// <summary>
        ///     reads a http content as a json object
        /// </summary>
        /// <param name="content">http content that contains json</param>
        /// <returns>a json object parsed from the content</returns>
        /// <exception cref="JsonReaderException">thrown when the parser encounters an error</exception>
        public static async Task<JObject> ReadAsJObjectAsync(this HttpContent content)
        {
            string strContent = await content.ReadAsStringAsync();
            return JObject.Parse(strContent);
        }

    }

}