﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RumorFinder.Foundation.Extensions.System.Collections.Generic
{

    internal static class EnumerableExtensions
    {

        /// <summary>
        ///     allows tasks to be processed as they complete
        /// </summary>
        /// <param name="tasks">an enumeration of tasks</param>
        /// <returns>an enumeration of tasks, the order of which is the order of completion for each task</returns>
        public static IEnumerable<Task> Interleaved(this IEnumerable<Task> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<bool>[inputTasks.Count];
            var results = new Task[buckets.Length];
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new TaskCompletionSource<bool>();
                results[i] = buckets[i].Task;
            }

            int nextTaskIndex = -1;

            void Continuation(Task completed)
            {
                TaskCompletionSource<bool> bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                if (completed.IsFaulted)
                    bucket.TrySetException(completed.Exception?.InnerExceptions ?? Enumerable.Empty<Exception>());
                else if (completed.IsCanceled)
                    bucket.TrySetCanceled();
                else
                    bucket.TrySetResult(true);
            }

            foreach (Task inputTask in inputTasks)
            {
                inputTask.ContinueWith(Continuation,
                                       CancellationToken.None,
                                       TaskContinuationOptions.ExecuteSynchronously,
                                       TaskScheduler.Default);
            }

            return results;
        }

        /// <summary>
        ///     allows tasks to be processed as they complete
        /// </summary>
        /// <param name="tasks">an enumeration of tasks</param>
        /// <returns>an enumeration of tasks, the order of which is the order of completion for each task</returns>
        public static IEnumerable<Task<TResult>> Interleaved<TResult>(this IEnumerable<Task<TResult>> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<TResult>[inputTasks.Count];
            var results = new Task<TResult>[buckets.Length];
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new TaskCompletionSource<TResult>();
                results[i] = buckets[i].Task;
            }

            int nextTaskIndex = -1;

            void Continuation(Task<TResult> completed)
            {
                TaskCompletionSource<TResult> bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                if (completed.IsFaulted)
                    bucket.TrySetException(completed.Exception?.InnerExceptions ?? Enumerable.Empty<Exception>());
                else if (completed.IsCanceled)
                    bucket.TrySetCanceled();
                else
                    bucket.TrySetResult(completed.Result);
            }

            foreach (Task<TResult> inputTask in inputTasks)
            {
                inputTask.ContinueWith(Continuation,
                                       CancellationToken.None,
                                       TaskContinuationOptions.ExecuteSynchronously,
                                       TaskScheduler.Default);
            }

            return results;
        }

    }

}