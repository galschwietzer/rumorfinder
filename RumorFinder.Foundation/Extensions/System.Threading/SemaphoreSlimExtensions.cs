﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RumorFinder.Foundation.Extensions.System.Threading
{

    internal static class SemaphoreSlimExtensions
    {

        /// <summary>
        ///     locks asynchronously the semaphore slim and executes the action
        /// </summary>
        /// <param name="semaphore">the lock to use</param>
        /// <param name="action">the action to be executed inside the "lock block"</param>
        /// <param name="cancellationToken">the token used to cancel the wait of the semaphore</param>
        /// <returns>a task that will be completed when the action is done</returns>
        public static async Task LockAsync(
            this SemaphoreSlim semaphore,
            Action action,
            CancellationToken cancellationToken = default)
        {
            try
            {
                await semaphore.WaitAsync(cancellationToken);
                action();
            }
            finally
            {
                semaphore.TryRelease();
            }
        }

        /// <summary>
        ///     locks asynchronously the semaphore slim and executes the action
        /// </summary>
        /// <param name="semaphore">the lock to use</param>
        /// <param name="taskSupplier">the action to retrieve the task to be executed inside the "lock block"</param>
        /// <param name="cancellationToken">the token used to cancel the wait of the semaphore</param>
        /// <returns>a task that will be completed when the action is done</returns>
        public static async Task LockAsync(
            this SemaphoreSlim semaphore,
            Func<Task> taskSupplier,
            CancellationToken cancellationToken = default)
        {
            try
            {
                await semaphore.WaitAsync(cancellationToken);
                await taskSupplier();
            }
            finally
            {
                semaphore.TryRelease();
            }
        }

        /// <summary>
        ///     Attempts to release the semaphore.
        /// </summary>
        /// <param name="semaphore">The semaphore to release.</param>
        /// <param name="releaseCount">the release count for the release action.</param>
        /// <returns>true if the semaphore was released successfully, false otherwise.</returns>
        public static bool TryRelease(this SemaphoreSlim semaphore, int releaseCount = 1)
        {
            try
            {
                semaphore.Release(releaseCount);
                return true;
            }
            catch (SemaphoreFullException)
            {
                return false;
            }
        }

    }

}