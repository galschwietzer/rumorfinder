﻿using System;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTextSummarizer;
using RumorFinder.Foundation.Util;
using RumorFinder.NativeHttp;

namespace RumorFinder.Foundation
{

    /// <summary>
    ///     The main class of the foundation project. Represents an unreleased technological product.
    ///     with the methods in this class, rumors about the product can be searched/updated and presented to a user.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Product : IComparable<Product>
    {

        #region Static Fields and Constants

        private const string SearchQueryFormat = "News and Rumors about the upcoming {0}";

        private static readonly TimeSpan RumorsValidationTimeSpan = TimeSpan.FromHours(2);
        private static readonly TimeSpan ForegroundTimeoutSpan = TimeSpan.FromSeconds(15);
        private static readonly TimeSpan BackgroundTimeoutSpan = TimeSpan.FromSeconds(20);

        private static ImmutableList<Uri> InternalBlackListedSources = ImmutableList<Uri>.Empty;

        private static SummarizerArguments InternalArguments;

        private static readonly HttpProgressClient BackgroundClient;
        private static readonly HttpProgressClient ForegroundClient;

        #endregion

        #region Fields

        private readonly SemaphoreSlim _inflightOperationLock = new SemaphoreSlim(1, 1);

        [JsonProperty]
        private Rumors _internalRumors;

        #endregion

        #region Properties

        /// <summary>
        ///     a list of sources that cannot be used to find rumors from
        /// </summary>
        public static ImmutableList<Uri> BlackListedSources
        {
            get => Product.InternalBlackListedSources;
            set => Interlocked.Exchange(ref Product.InternalBlackListedSources, value);
        }

        [JsonProperty]
        public string Name { get; private set; }

        public Rumors Rumors
        {
            get => _internalRumors;
            private set
            {
                if (_internalRumors?.Equals(value) == true)
                    return;
                Interlocked.Exchange(ref _internalRumors, value);
                OnRumorsChanged();
            }
        }

        #endregion

        #region Constructors

        static Product()
        {
            // set up the http clients to use native interfaces
            Product.BackgroundClient = new NativeHttpProgressClient(new NativeMessageHandler())
            {
                Timeout = Product.BackgroundTimeoutSpan
            };

            Product.ForegroundClient = new NativeHttpProgressClient(new NativeMessageHandler())
            {
                Timeout = Product.ForegroundTimeoutSpan
            };

            // set up the summarizing arguments
            Product.InternalArguments = new SummarizerArguments
            {
                MaxSummarySizeInPercent = 100,
                MaxSummarySentences = 2
            };
        }

        [JsonConstructor]
        private Product()
        { }

        public Product(string name)
        {
            Name = name;
            Rumors = Rumors.Empty;
        }

        #endregion

        #region Events

        /// <summary>
        ///     an event that is called every time the rumors object is changed
        /// </summary>
        public event EventHandler RumorsChanged;

        #endregion

        /// <summary>
        ///     changes the default summarizer arguments for the entire product class
        /// </summary>
        /// <param name="arguments"></param>
        public static void SetSummarizerArguments(SummarizerArguments arguments) =>
            Interlocked.Exchange(ref Product.InternalArguments, arguments);

        /// <summary>
        ///     Finds new rumors about this product by searching for rumor articles, parsing their text and and summarizing them.
        /// </summary>
        /// <param name="isUserInitiated">a bool indicating if the operation was initiated by the user</param>
        /// <param name="cancellationToken">the token used to cancel the operation</param>
        /// <param name="progress">
        ///     the object used to report the progress of the ongoing operation. The progress is reported in the
        ///     range 0-1.
        /// </param>
        /// <returns>
        ///     The status of the completed operation.
        ///     if the operation hasn't failed the found rumors will override the current rumors.
        /// </returns>
        public async Task<OperationStatus> FindRumorsAsync(
            bool isUserInitiated = true,
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null)
        {
            HttpProgressClient client = isUserInitiated ? Product.ForegroundClient : Product.BackgroundClient;

            OperationStatus status = OperationStatus.Failed;

            async Task PerformFetch()
            {
                RumorFetch fetch = await FetchRumorsAsync(client,
                                                          cancellationToken,
                                                          progress).ConfigureAwait(false);

                status = fetch.Status;

                // only override the old rumors if the operation hasn't failed
                if (status != OperationStatus.Failed)
                    Rumors = fetch.Data;
            }

            // execute the fetch in a synchronized fashion 
            //await _inflightOperationLock.LockAsync(PerformFetch, cancellationToken).ConfigureAwait(false);
            await PerformFetch().ConfigureAwait(false);
            return status;
        }

        /// <summary>
        ///     Update the current rumors with newly found information from web articles.
        /// </summary>
        /// <param name="isUserInitiated">a bool indicating if the operation was initiated by the user.</param>
        /// <param name="cancellationToken">the token used to cancel the operation.</param>
        /// <param name="progress">
        ///     the object used to report the progress of the ongoing operation. The progress is reported in the
        ///     range 0-1.
        /// </param>
        /// <returns>
        ///     The status of the completed operation.
        ///     if the operation hasn't failed, any new information found will be added to
        ///     the current rumors.
        /// </returns>
        public async Task<OperationStatus> UpdateRumorsAsync(
            bool isUserInitiated = true,
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null)
        {
            // we yield instantly to not allow certain deadlocks 
            // in ProductList methods
            await Task.Yield();

            HttpProgressClient client = isUserInitiated ? Product.ForegroundClient : Product.BackgroundClient;

            // fetch rumors from the web and update the current rumors with any new information found
            try
            {
                // synchronize the operation
                await _inflightOperationLock.WaitAsync(cancellationToken).ConfigureAwait(false);

                // fetch new rumors
                RumorFetch fetch = await FetchRumorsAsync(client, cancellationToken, progress).ConfigureAwait(false);

                // if the fetch failed we return
                if (fetch.Status is OperationStatus.Failed)
                    return OperationStatus.Failed;

                // try to find new information
                Rumors = Rumors.UpdateFrom(fetch.Data, out bool foundUpdates);

                // return the status accordingly
                return foundUpdates ? OperationStatus.Successful : OperationStatus.Unsuccessful;
            }
            finally
            {
                // make sure to release the lock
                _inflightOperationLock.Release();
            }
        }

        /// <summary>
        ///     Called when the user has seen the updates for this product
        /// </summary>
        /// <remarks>This operation usually completes synchronously</remarks>
        /// <param name="cancellationToken">A token used to cancel the operation</param>
        public async Task UpdatesSeenAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                // we enter the lock because we are changing the rumors property
                await _inflightOperationLock.WaitAsync(cancellationToken);

                // merge the updates into the content
                Rumors = Rumors.MergeUpdates();
            }
            finally
            {
                // make sure to release the lock
                _inflightOperationLock.Release();
            }
        }


        #region Private Helpers

        /// <summary>
        ///     raises the <c>RumorsChanged</c> event.
        /// </summary>
        private void OnRumorsChanged() =>
            RumorsChanged?.Invoke(this, EventArgs.Empty);

        #endregion

        #region Comparable and object methods

        public int CompareTo(Product other) => string.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);

        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj) => obj is Product p && Name.Equals(p.Name, StringComparison.OrdinalIgnoreCase);

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => Name.ToLower().GetHashCode();

        #endregion

    }

}