﻿using System;

namespace RumorFinder.Foundation.Recommendations
{
    public class RecommenderException : Exception
    {
        #region Static Fields and Constants

        private const string TimeoutMessage = "The request has timedout";
        private const string FailedRequestMessage = "The request has failed";
        private const string BadIDMessage = "The given ID is not a valid UUID V4 ID";

        #endregion

        #region Properties

        public static RecommenderException TimeoutException =>
            new RecommenderException(RecommenderException.TimeoutMessage);

        public static RecommenderException FailedRequestException =>
            new RecommenderException(RecommenderException.FailedRequestMessage);

        public static RecommenderException BadIDException =>
            new RecommenderException(RecommenderException.BadIDMessage);

        #endregion

        #region Constructors

        public RecommenderException()
        { }

        public RecommenderException(string message) : base(message)
        { }

        public RecommenderException(string message, Exception innerException) : base(message, innerException)
        { }

        #endregion
    }
}