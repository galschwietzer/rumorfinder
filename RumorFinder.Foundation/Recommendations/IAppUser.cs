﻿using System.Collections.Generic;

namespace RumorFinder.Foundation.Recommendations
{

    /// <summary>
    ///     Represents a user of the RumorFinder app
    /// </summary>
    public interface IAppUser
    {

        #region Properties

        /// <summary>
        ///     The id of the user. Must be a version 4 RFC 4122 compliant UUID.
        /// </summary>
        string ID { get; } 

        /// <summary>
        ///     Products searched by the user.
        /// </summary>
        IEnumerable<Product> SearchedProducts { get; }

        /// <summary>
        ///     Products the user follows.
        /// </summary>
        IEnumerable<Product> FollowedProducts { get; }

        /// <summary>
        ///     Products that were recommended to the user but the user dismissed them.
        /// </summary>
        IEnumerable<Product> DeclinedRecommendations { get; }

        #endregion

    }

}