﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RumorFinder.Foundation.Util;

namespace RumorFinder.Foundation.Recommendations
{
    /// <summary>
    ///     This class communicates with the RumorFinder.RecommenderSystem server to find product recommendations for a user
    ///     based on his use of the RumorFinder application.
    /// </summary>
    public static class Recommender
    {
        #region Static Fields and Constants

        private const string IDLocation = "id";
        private const string PreferencesLocation = "preferences";
        private const string SearchHistoryLocation = "search history";
        private const string FollowedProductsLocation = "followed products";
        private const string DeclinedRecommendationsLocation = "declined recommendations";

        private const string IDRegex = "[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}";

        private static readonly HttpClient DefaultClient = new NativeHttpProgressClient
        {
            Timeout = TimeSpan.FromSeconds(5)
        };

        private static readonly Uri ApiEndpoint = new Uri("https://api.rumorfinder.com/recommendations/");

        #endregion


        /// <summary>
        ///     Returns appropriate recommendations for the given user from a recommender system in a remote server.
        /// </summary>
        /// <param name="user">The user the recommendations will be given for.</param>
        /// <param name="cancellationToken">The token used to cancel the operation.</param>
        /// <param name="progress">The object used to report back the progress of the operation.</param>
        /// <param name="client">
        ///     The object used to execute the call to the remote server. When a client is not passed a default
        ///     one is used.
        /// </param>
        /// <returns>A list of recommended products for the user</returns>
        /// <exception cref="RecommenderException">Thrown when an error occurs in the request process</exception>
        public static async Task<IEnumerable<Product>> GetRecommendationsAsync(
            IAppUser user,
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null,
            HttpClient client = null)
        {
            client = client ?? Recommender.DefaultClient;

            // ensure that the user's id is a valid UUID V4 id
            if (!user.ID.IsValid()) throw RecommenderException.BadIDException;

            try
            {
                var content = new StringContent(Serialize(user));

                HttpResponseMessage responseMessage =
                    await client.PostAsync(Recommender.ApiEndpoint, content, cancellationToken, progress);

                responseMessage.EnsureSuccessStatusCode();

                string response = await responseMessage.Content.ReadAsStringAsync();
                var recommendations = JsonConvert.DeserializeObject<IEnumerable<string>>(response);

                return recommendations.Select(recommendation => new Product(recommendation));
            }
            catch (HttpRequestException)
            {
                throw RecommenderException.FailedRequestException;
            }
            catch (OperationCanceledException)
            {
                //if the exception was caused by the token, throw the original exception
                if (cancellationToken.IsCancellationRequested)
                    throw;

                //if the exception did not originate from the token, it was due to a timeout
                //so we throw a RecommenderException instead
                throw RecommenderException.TimeoutException;
            }
        }


        /// <summary>
        ///     Serializes the user's information in the JSON format.
        /// </summary>
        /// <param name="user">The user who's data will be serialized/</param>
        /// <returns>The user's information serialized into a string.</returns>
        private static string Serialize(IAppUser user)
        {
            var id = new JProperty(Recommender.IDLocation, user.ID);

            // we make sure to not add any duplicates
            // the order of the method calls is important because if a product was
            // both searched and followed by the user the following takes precedence 
            // over the search. The hierarchy is: following > search > declined recommendations
            var addedProducts = new HashSet<string>();
            var followedProducts = new JProperty(Recommender.FollowedProductsLocation,
                                                 SerializeEnumerable(user.FollowedProducts));
            var searchHistory = new JProperty(Recommender.SearchHistoryLocation,
                                              SerializeEnumerable(user.SearchedProducts));
            var declinedRecommendations = new JProperty(Recommender.DeclinedRecommendationsLocation,
                                                        SerializeEnumerable(user.DeclinedRecommendations));
            var preferences = new JProperty(Recommender.PreferencesLocation, 
                                            new JObject(followedProducts, declinedRecommendations, searchHistory));

            return new JObject(id, preferences).ToString();


            // Serializes a sequence of products by only selecting their names
            JArray SerializeEnumerable(IEnumerable<Product> products) => new JArray(from p in products
                                                                                    where addedProducts.Add(p.Name.ToLower())
                                                                                    select new JValue(p.Name));
        }

        /// <summary>
        ///     Helper method to ensure that if we can report the progress of the request, we do.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestUri"></param>
        /// <param name="content"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        private static Task<HttpResponseMessage> PostAsync(
            this HttpClient client,
            Uri requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            if (progress != null && client is HttpProgressClient progressClient)
                return progressClient.PostAsync(requestUri, content, cancellationToken, progress);

            return client.PostAsync(requestUri, content, cancellationToken);
        }

        /// <summary>
        ///     checks if the given id is a valid UUID V4 id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static bool IsValid(this string id) => Regex.IsMatch(id.ToUpper(), Recommender.IDRegex);
    }
}