﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RumorFinder.Foundation.Extensions.System.Net.Http;
using RumorFinder.Foundation.Properties;
using RumorFinder.Foundation.Util.Data_Structures;

namespace RumorFinder.Foundation.Util
{

    /// <summary>
    ///     represents a search api that retrieves search result for a given search terms
    /// </summary>
    internal static class SearchApi
    {

        #region Static Fields and Constants

        private const string ApiAddress = "https://www.googleapis.com/customsearch/";

        private const string ApiVersion = "v1";
        private const string ExcludedSite = "youtube.com";
        private const string ExcludedState = "e";
        private const string IncludedFields = "spelling, items(title, link)";
        private const string SafeSearchLevel = "high";
        private const string ResultCount = "10";
        private const string SortingExpression = "date";

        private const string UrlLocation = "link";
        private const string HeadlineLocation = "title";
        private const string SpellingLocation = "spelling/correctedQery";
        private const string SearchResultsLocation = "items";

        // ReSharper disable once InconsistentNaming - two word acronym
        private static readonly string SearchEngineID = Resources.SearchEngineID;
        private static readonly string ApiKey = Resources.SearchApiKey;

        private static readonly HttpProgressClient ApiClient = new NativeHttpProgressClient();

        private static readonly (string Name, string Value)[] RequestParameters =
        {
            ("q", string.Empty),
            ("num", SearchApi.ResultCount),
            ("cx", SearchApi.SearchEngineID),
            ("key", SearchApi.ApiKey),
            ("siteSearch", SearchApi.ExcludedSite),
            ("siteSearchFilter", SearchApi.ExcludedState),
            ("fields", SearchApi.IncludedFields),
            ("safeSearch", SearchApi.SafeSearchLevel),
            ("sort", SearchApi.SortingExpression)
        };

        private static readonly string RequestFormat;

        #endregion

        #region Constructors

        static SearchApi()
        {
            //builds the request format
            var requestFormat = new StringBuilder($"{SearchApi.ApiAddress}{SearchApi.ApiVersion}");
            var connector = "";
            foreach ((string name, string value) in SearchApi.RequestParameters)
            {
                connector = string.IsNullOrEmpty(connector) ? "?" : "&";
                string v = value;
                if (name == "q") v = "{0}";
                requestFormat.Append($"{connector}{name}={v}");
            }

            SearchApi.RequestFormat = requestFormat.ToString();
        }

        #endregion

        /// <summary>
        ///     returns the search results for the given search terms ordered by date
        /// </summary>
        /// <param name="apiCall">the api call to the search api</param>
        /// <returns>the search results</returns>
        /// <exception cref="SearchApiException">
        ///     throws when the api call is unsuccessful
        /// </exception>
        private static async Task<IEnumerable<SearchResult>> GetSearchResultsAsyncCore(
            Task<HttpResponseMessage> apiCall)
        {
            HttpResponseMessage apiResponse;
            try
            {
                apiResponse = await apiCall.ConfigureAwait(false);
                apiResponse.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                throw SearchApiException.FailedRequestException;
            }

            JObject jsonResponse;
            try
            {
                jsonResponse = await apiResponse.Content.ReadAsJObjectAsync().ConfigureAwait(false);
            }
            catch (JsonReaderException)
            {
                throw SearchApiException.FailedRequestException;
            }

            var results = jsonResponse[SearchApi.SearchResultsLocation] as JArray;

            if (results is null)
                return Enumerable.Empty<SearchResult>();

            var searchResults = from result in results //parse the response for the search results
                                let uri = result[SearchApi.UrlLocation].ToString()
                                let headline = result[SearchApi.HeadlineLocation].ToString()
                                select new SearchResult(new Uri(uri), headline);

            return searchResults;
        }

        #region Private Helpers

        /// <summary>
        ///     the methods return the url for an api response
        ///     according to the given search terms and the maximum number of results
        /// </summary>
        /// <param name="searchTerms"> the search terms that the search results will be returned for</param>
        /// <returns>the uri for the api response with the given parameters</returns>
        private static Uri BuildApiRequest(string searchTerms) =>
            new Uri(string.Format(SearchApi.RequestFormat, searchTerms));

        #endregion

        #region Basic Method Overloads

        /// <summary>
        ///     returns the search results for the given search terms ordered by date
        /// </summary>
        /// <remarks>
        ///     if the api returns a spelling correction to the search terms
        ///     <code>shouldUseCorrectedSearchTerms</code> is called with the correctly spelled search terms.
        ///     if it returns true the search results will be returned for the corrected search terms and searchTerms is reassigned
        ///     for
        ///     the corrected search terms, if not
        ///     the original search terms will be used for the results.
        ///     if the api call is un-successful an empty array will be returned as the results
        /// </remarks>
        /// <param name="searchTerms">
        ///     a non empty or null string, the search terms for which the search results will be returned
        ///     for
        /// </param>
        /// <param name="cancellationToken">the cancellation token used to cancel the operation</param>
        /// <param name="client">the http client to make the request with</param>
        /// <returns>the search terms and their search results</returns>
        /// <exception cref="HttpRequestException">
        ///     throws when the request to the server fails
        /// </exception>
        public static async Task<IEnumerable<SearchResult>> GetSearchResultsAsync(
            string searchTerms,
            CancellationToken cancellationToken,
            HttpClient client = null)
        {
            client = client ?? SearchApi.ApiClient;

            try
            {
                //complete the api call and parse the response
                Task<HttpResponseMessage> apiCall =
                    client.GetAsync(BuildApiRequest(searchTerms), cancellationToken);
                return await GetSearchResultsAsyncCore(apiCall).ConfigureAwait(false);
            }
            catch (OperationCanceledException)
            {
                //if the exception was caused by the token, throw the original exception
                if (cancellationToken.IsCancellationRequested)
                    throw;

                //if the exception did not originate from the token, it was due to a timeout
                //so we throw a SearchApiException instead
                throw SearchApiException.TimeoutException;
            }
        }

        /// <summary>
        ///     returns the search results for the given search terms ordered by date
        /// </summary>
        /// <param name="searchTerms">
        ///     a non empty or null string, the search terms for which the search results will be returned
        ///     for
        /// </param>
        /// <param name="cancellationToken">the cancellation token used to cancel the operation</param>
        /// <param name="progress">the object used to report progress</param>
        /// <param name="client">the http client to make the request with</param>
        /// <returns>the search terms and their search results</returns>
        /// <exception cref="SearchApiException">
        ///     throws when the request to the server fails or times out
        /// </exception>
        public static async Task<IEnumerable<SearchResult>> GetSearchResultsAsync(
            string searchTerms,
            CancellationToken cancellationToken,
            IProgress<float> progress,
            HttpProgressClient client = null)
        {
            client = client ?? SearchApi.ApiClient;

            try
            {
                //complete the api call and parse the response
                Task<HttpResponseMessage> apiCall =
                    client.GetAsync(BuildApiRequest(searchTerms), cancellationToken, progress);
                return await GetSearchResultsAsyncCore(apiCall).ConfigureAwait(false);
            }
            catch (OperationCanceledException)
            {
                //if the exception was caused by the token, throw the original exception
                if (cancellationToken.IsCancellationRequested)
                    throw;

                //if the exception did not originate from the token, it was due to a timeout
                //so we throw a SearchApiException instead
                throw SearchApiException.TimeoutException;
            }
        }

        #endregion

    }

}