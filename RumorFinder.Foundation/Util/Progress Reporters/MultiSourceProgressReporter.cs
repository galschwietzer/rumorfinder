﻿using System;
using System.Linq;

namespace RumorFinder.Foundation.Util.Progress_Reporters
{

    /// <summary>
    ///     Represents a progress reporter from multiple sources
    /// </summary>
    internal class MultiSourceProgressReporter
    {

        #region Fields

        private readonly int _maxSources;
        private readonly IProgress<float> _progress;
        private readonly float[] _progressSources;
        private int _itemCount;

        #endregion

        #region Constructors

        public MultiSourceProgressReporter(IProgress<float> progress, int numSources)
        {
            _progress = progress;
            _progressSources = new float[numSources];
            _maxSources = numSources;
            _itemCount = 0;
        }

        #endregion

        /// <summary>
        ///     Adds a new progress source if threes room
        /// </summary>
        /// <param name="progressSource">a new progress source</param>
        /// <returns>true if the source was added, false otherwise</returns>
        public bool TryAddSource(Progress<float> progressSource)
        {
            if (_itemCount == _maxSources) return false;
            int index = _itemCount++;
            progressSource.ProgressChanged += (sender, progress) =>
            {
                lock (_progressSources.SyncRoot)
                {
                    _progressSources[index] = progress;
                    OnUpdate();
                }
            };
            return true;
        }

        /// <summary>
        ///     called when threes a new progress value available
        /// </summary>
        /// <remarks>should be called only inside a lock</remarks>
        private void OnUpdate()
        {
            float totalProgress = _progressSources.Sum();
            _progress.Report(totalProgress / _maxSources);
        }

    }

}