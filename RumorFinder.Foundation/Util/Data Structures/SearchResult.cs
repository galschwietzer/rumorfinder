﻿using System;

namespace RumorFinder.Foundation.Util.Data_Structures
{

    /// <summary>
    ///     Represents an internet search result
    /// </summary>
    internal readonly struct SearchResult
    {

        public Uri Uri { get; }
        public string Headline { get; }

        /// <summary>
        ///     creates an instance of search result
        /// </summary>
        /// <param name="uri">the uri of the search result</param>
        /// <param name="headline">the headline of the search result</param>
        public SearchResult(Uri uri, string headline)
        {
            Uri = uri;
            Headline = headline;
        }

        public void Deconstruct(out Uri uri, out string headline) =>
            (uri, headline) = (Uri, Headline);

        public override string ToString() =>
            $"Uri = {Uri}, Headline = {Headline}";

    }

}