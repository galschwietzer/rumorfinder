﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Punchclock;
using RumorFinder.Foundation.Extensions.System.Collections.Generic;
using RumorFinder.Foundation.Util.Data_Structures;
using RumorFinder.Foundation.Util.Progress_Reporters;

namespace RumorFinder.Foundation.Util
{
    /// <summary>
    ///     represents an Api that parses articles
    /// </summary>
    internal static class ArticleParserApi
    {
        #region Static Fields and Constants

        private const string ApiAddress = "https://api.RumorFinder.com/parse";

        private static readonly HttpProgressClient ApiClient = new NativeHttpProgressClient();
        private static readonly OperationQueue ApiQueue = new OperationQueue();

        private static readonly ImmutableList<string> IgnoredNodes = ImmutableList.CreateRange(new[]
        {
            "h1", "h2", "h3", "h4", "h5", "h6", "dir", "dt", "dt", "li", "dl", "ol", "ul"
        });

        #endregion

        /// <summary>
        ///     parses text and metadata from the given article
        /// </summary>
        /// <remarks>
        ///     if content cant be found, an empty enumerable is returned instead.
        ///     if the domain cant be found an empty string is returned instead
        /// </remarks>
        /// <param name="apiCall">the api call to the extraction Api</param>
        /// <returns>the text in the form of paragraphs and other metadata inside a <code>ParsedArticle</code> struct</returns>
        /// <exception cref="ArticleParserApiException">
        ///     throws when the api call is unsuccessful
        /// </exception>
        private static async Task<ParsedArticle> ParseArticleAsyncCore(Task<HttpResponseMessage> apiCall)
        {
            HttpResponseMessage apiResponse;
            try
            {
                Task<HttpResponseMessage> call = apiCall;
                apiCall = ArticleParserApi.ApiQueue.Enqueue(1, () => call);

                apiResponse = await apiCall.ConfigureAwait(false);
                apiResponse.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                throw ArticleParserApiException.FailedRequestException;
            }

            string parsedContent;
            try
            {
                parsedContent = await apiResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
            catch (JsonReaderException)
            {
                throw ArticleParserApiException.FailedRequestException;
            }

            IEnumerable<string> paragraphs = ExtractParagraphs(parsedContent);

            return new ParsedArticle(paragraphs);
        }

        /// <summary>
        ///     awaits all the extraction tasks, handling the exceptions
        /// </summary>
        /// <remarks>in case a request fails, due to a bad/failed connection or a timeout, null us returned as the result</remarks>
        /// <param name="extractionTasks">the extraction tasks for the articles</param>
        /// <param name="cancellationToken">the cancellation token used to cancel the operation</param>
        /// <returns>an enumeration of the parsed articles</returns>
        private static async Task<IEnumerable<ParsedArticle?>> ParseArticlesAsyncCore(
            IEnumerable<Task<ParsedArticle>> extractionTasks,
            CancellationToken cancellationToken)
        {
            var parsedArticles = new List<ParsedArticle?>();
            foreach (Task<ParsedArticle> extractionTask in extractionTasks.Interleaved())
            {
                ParsedArticle? result;
                try
                {
                    result = await extractionTask;
                }
                catch (ArticleParserApiException)
                {
                    //the api call failed for some reason
                    result = null;
                }
                catch (OperationCanceledException)
                {
                    //do throw if cancellation was requested
                    if (cancellationToken.IsCancellationRequested)
                        throw;
                    //but do handle in case of timeout
                    result = null;
                }

                parsedArticles.Add(result);
            }

            return parsedArticles;
        }

        #region Basic Method Overloads

        /// <summary>
        ///     returns the extracted text from the given article uri
        /// </summary>
        /// <remarks>
        ///     if content cant be found, an empty enumerable is returned instead.
        ///     if the domain cant be found an empty string is returned instead
        /// </remarks>
        /// <param name="articleUri">the article uri to extract the text from</param>
        /// <param name="cancellationToken">the cancellation token used to cancel the extraction</param>
        /// <param name="client">the HttpClient to make the requests with</param>
        /// <returns>the extracted text from the article</returns>
        public static Task<ParsedArticle> ParseArticleAsync(
            Uri articleUri,
            CancellationToken cancellationToken,
            HttpClient client = null)
        {
            client = client ?? ArticleParserApi.ApiClient;
            Uri apiRequestUri = BuildRequestUri(articleUri);
            return ParseArticleAsyncCore(client.GetAsync(apiRequestUri, cancellationToken));
        }

        /// <summary>
        ///     returns the extracted text from the given article uri
        /// </summary>
        /// <remarks>
        ///     if content cant be found, an empty enumerable is returned instead.
        ///     if the domain cant be found an empty string is returned instead
        /// </remarks>
        /// <param name="articleUri">the article uri to extract the text from</param>
        /// <param name="cancellationToken">the cancellation token used to cancel the extraction</param>
        /// <param name="progress">the object used to report progress back to</param>
        /// <param name="client">the HttpClient to make the requests with</param>
        /// <returns>the extracted text from the article</returns>
        public static Task<ParsedArticle> ParseArticleAsync(
            Uri articleUri,
            CancellationToken cancellationToken,
            IProgress<float> progress,
            HttpProgressClient client = null)
        {
            client = client ?? ArticleParserApi.ApiClient;
            Uri apiRequestUri = BuildRequestUri(articleUri);
            return ParseArticleAsyncCore(client.GetAsync(apiRequestUri, cancellationToken, progress));
        }

        /// <summary>
        ///     parses the articles at the given uris
        /// </summary>
        /// <remarks>
        ///     the order of the returned objects is not guaranteed.
        ///     in case a request fails, due to a bad/failed connection or a timeout, null is returned instead the result
        /// </remarks>
        /// <param name="articleUris">the article uris to parse</param>
        /// <param name="cancellationToken">the cancellation token used to cancel the operation</param>
        /// <param name="client">the client to make the requests with</param>
        /// <returns>an enumeration of the parsed articles</returns>
        public static Task<IEnumerable<ParsedArticle?>> ParseArticlesAsync(
            IEnumerable<Uri> articleUris,
            CancellationToken cancellationToken,
            HttpClient client = null)
        {
            client = client ?? ArticleParserApi.ApiClient;
            IEnumerable<Task<ParsedArticle>> extractionTasks = from uri in articleUris
                                                               select ParseArticleAsync(uri, cancellationToken, client);
            return ParseArticlesAsyncCore(extractionTasks, cancellationToken);
        }

        /// <summary>
        ///     parses the articles at the given uris
        /// </summary>
        /// <remarks>
        ///     the order of the returned objects is not guaranteed.
        ///     in case a request fails, due to a bad/failed connection or a timeout, null is returned instead the result
        /// </remarks>
        /// <param name="articleUris">the article uris to parse</param>
        /// <param name="cancellationToken">the cancellation token used to cancel the operation</param>
        /// <param name="progress">the object used to report the progress of the operation</param>
        /// <param name="client">the client to make the requests with</param>
        /// <returns>an enumeration of the parsed articles</returns>
        public static Task<IEnumerable<ParsedArticle?>> ParseArticlesAsync(
            IEnumerable<Uri> articleUris,
            CancellationToken cancellationToken,
            IProgress<float> progress,
            HttpProgressClient client = null)
        {
            client = client ?? ArticleParserApi.ApiClient;

            //mitigate any lazy enumerable we might receive
            articleUris = articleUris as IList<Uri> ?? articleUris.ToList();
            int length = articleUris.Count();

            var extractionTasks = new List<Task<ParsedArticle>>(length);
            var reporter = new MultiSourceProgressReporter(progress, length);

            foreach (Uri articleUri in articleUris)
            {
                //connect each of the individual progress objects to the main method one 
                var proxyProgress = new Progress<float>();
                reporter.TryAddSource(proxyProgress);
                extractionTasks.Add(ParseArticleAsync(articleUri, cancellationToken, proxyProgress, client));
            }

            return ParseArticlesAsyncCore(extractionTasks, cancellationToken);
        }

        #endregion

        #region Private Helpers

        /// <summary>
        ///     builds the api request uri with the given article uri
        /// </summary>
        /// <param name="articleUri">the article uri to send to the api</param>
        /// <returns>the api request uri</returns>
        private static Uri BuildRequestUri(Uri articleUri)
        {
            string requestUri = $"{ArticleParserApi.ApiAddress}?url={articleUri.AbsoluteUri}";
            return new Uri(requestUri);
        }

        /// <summary>
        ///     extracts the paragraphs from parsed html content
        /// </summary>
        /// <remarks>in the case no viable paragraphs are found in the content an empty enumerable is returned</remarks>
        /// <param name="parsedContent">the parsed html content of an article</param>
        /// <returns>the paragraphs that were present in the content</returns>
        private static IEnumerable<string> ExtractParagraphs(string parsedContent)
        {
            // extract the html from the response
            var doc = new HtmlDocument();
            doc.LoadHtml(parsedContent);

            var extractedText = new StringWriter();
            ConvertHtml(doc.DocumentNode, extractedText);
            extractedText.Flush();
            return extractedText.ToString().Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);

            //writes the text of the html node to the writer
            void ConvertHtml(HtmlNode node, TextWriter outText)
            {
                switch (node.NodeType)
                {
                    case HtmlNodeType.Comment:
                        // don't output comments
                        break;

                    case HtmlNodeType.Document:
                        ConvertSubNodes(node, outText);
                        break;

                    case HtmlNodeType.Text:

                        // script and style must not be output
                        string parentName = node.ParentNode.Name;
                        if (parentName == "script" || parentName == "style")
                            break;

                        // get text
                        string html = ((HtmlTextNode) node).Text;

                        // is it in fact a special closing node output as text?
                        if (HtmlNode.IsOverlappedClosingElement(html))
                            break;

                        // check the text is meaningful and not a bunch of whitespaces
                        if (html.Trim().Length > 0) outText.Write(HtmlEntity.DeEntitize(html));
                        break;

                    case HtmlNodeType.Element:
                        if (node.Name == "p")
                        {
                            // treat paragraphs as newLine
                            outText.Write(Environment.NewLine);
                        }

                        if (node.HasChildNodes) ConvertSubNodes(node, outText);
                        break;
                }
            }

            //writes the text of all relevant sub nodes
            void ConvertSubNodes(HtmlNode node, TextWriter outText)
            {
                foreach (HtmlNode subNode in node.ChildNodes)
                {
                    //ignore the ignored nodes
                    if (ArticleParserApi.IgnoredNodes.Contains(subNode.Name))
                        continue;

                    ConvertHtml(subNode, outText);
                }
            }
        }

        #endregion Private Helpers  
    }
}