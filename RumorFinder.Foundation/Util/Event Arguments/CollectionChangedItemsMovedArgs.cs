﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace RumorFinder.Foundation.Util.Event_Arguments
{

    /// <summary>
    ///     Represents a collection changed event args for the Move event action
    /// </summary>
    public class CollectionChangedItemsMovedArgs : NotifyCollectionChangedEventArgs
    {

        #region Properties

        public IEnumerable<(int oldIndex, int newIndex)> Indexes { get; }

        #endregion

        #region Constructors

        public CollectionChangedItemsMovedArgs(IList movedObjects, IEnumerable<(int oldIndex, int newIndex)> indexes) :
            base(NotifyCollectionChangedAction.Move, movedObjects, indexes.FirstOrDefault().oldIndex, indexes.FirstOrDefault().newIndex)
        {
            Indexes = indexes;
        }

        #endregion

    }

}