﻿using System.Collections;
using System.Collections.Specialized;

namespace RumorFinder.Foundation.Util.Event_Arguments
{

    /// <summary>
    ///     Represents a collection changed event args for the Add event action
    /// </summary>
    public class CollectionChangedItemAddedArgs : NotifyCollectionChangedEventArgs
    {

        #region Properties

        public int NewItemIndex { get; }

        #endregion

        #region Constructors

        public CollectionChangedItemAddedArgs(object addedObject, int newItemIndex) :
            base(NotifyCollectionChangedAction.Add, new ArrayList {addedObject}, newItemIndex) =>
            NewItemIndex = newItemIndex;

        #endregion

    }

}