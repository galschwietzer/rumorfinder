﻿
## The Search Algorithm
The search process for the rumors starts when the [`
FindRumorsAsync`](xref:RumorFinder.Foundation.Product#RumorFinder_Foundation_Product_FindRumorsAsync_System_Boolean_System_Threading_CancellationToken_System_IProgress_System_Single__) method of the [`Product`](xref:RumorFinder.Foundation.Product) Class is called.

The search consists of a number of stages:
 1. Online articles about the unreleased product are searched using the [Google CSE Api](https://developers.google.com/custom-search/v1/overview) with the [`SearchApi`](xref:RumorFinder.Foundation.Util.SearchApi) Class.
 2. Filter the articles that are not considered rumor articles(That is decided according to the output of [`Classifier.ClassifyHeadline`](xref:RumorFinder.Foundation.Text.Classifier#RumorFinder_Foundation_Text_Classifier_ClassifyHeadline_System_String_System_String_)).
 3. The content of each article is extracted using the [RumorFinder.Server's Web Parser](server/index.html#the-web-parser) with the [`ArticleParserApi`](xref:RumorFinder.Foundation.Util.ArticleParserApi) Class.
 4. The content is divided into paragraphs, then each paragraph is classified(with the [`Classifier.ClassifyRumor`](xref:RumorFinder.Foundation.Text.Classifier#RumorFinder_Foundation_Text_Classifier_ClassifyRumor_System_String_System_String_) method) with a [`RumorCategory`](xref:RumorFinder.Foundation.RumorCategory).
 5. The paragraphs are grouped according to their category.
 6. Each category is summarized and placed in a[`Rumor`](xref:RumorFinder.Foundation.Rumor) object.
 7. All the [`Rumor`](xref:RumorFinder.Foundation.Rumor) objects are combined into a single [`Rumors`](xref:RumorFinder.Foundation.Rumors) object.
 8. The status of the operation is returned to the caller(as an [`OperationStatus`](xref:RumorFinder.Foundation.OperationStatus) enumeration) and the Rumors property is updated accordingly.

