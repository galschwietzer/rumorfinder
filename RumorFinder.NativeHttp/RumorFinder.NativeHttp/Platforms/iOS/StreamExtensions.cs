﻿using System;
using System.Buffers;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace RumorFinder.NativeHttp.Platforms.iOS
{

    internal static class StreamExtensions
    {

        /// <summary>
        ///     Asynchronously writes a sequence of bytes to the current stream, advances the current position within this stream
        ///     by the number of bytes written, and monitors cancellation requests.
        /// </summary>
        /// <remarks>
        ///     This method is being copied in its entirely from the dotnet/corefx repo because it is not yet available in .net
        ///     Standard.
        /// </remarks>
        /// <param name="stream"></param>
        /// <param name="buffer">The buffer to write data from.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is None.</param>
        /// <returns>A task that represents the asynchronous write operation.</returns>
        public static ValueTask WriteAsync(
            this Stream stream,
            ReadOnlyMemory<byte> buffer,
            CancellationToken cancellationToken = default)
        {
            //attempt to write directly from the buffer
            if (MemoryMarshal.TryGetArray(buffer, out ArraySegment<byte> array))
                return new ValueTask(stream.WriteAsync(array.Array, array.Offset, array.Count, cancellationToken));

            //if we cant copy to a shared buffer and write from it
            byte[] sharedBuffer = ArrayPool<byte>.Shared.Rent(buffer.Length);
            buffer.Span.CopyTo(sharedBuffer);
            return new
                ValueTask(FinishWriteAsync(stream.WriteAsync(sharedBuffer, 0, buffer.Length, cancellationToken),
                                           sharedBuffer));

            //make sure to return the shared buffer
            async Task FinishWriteAsync(Task writeTask, byte[] localBuffer)
            {
                try
                {
                    await writeTask.ConfigureAwait(false);
                }
                finally
                {
                    ArrayPool<byte>.Shared.Return(localBuffer);
                }
            }
        }

    }

}