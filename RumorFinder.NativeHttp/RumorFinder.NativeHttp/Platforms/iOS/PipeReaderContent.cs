﻿using System;
using System.Buffers;
using System.IO;
using System.IO.Pipelines;
using System.Net;
using System.Net.Http;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;

namespace RumorFinder.NativeHttp.Platforms.iOS
{

    /// <summary>
    ///     represents a HttpContent that reads its content from a Pipe
    /// </summary>
    internal class PipeReaderContent : HttpContent
    {

        #region Fields

        private readonly PipeReader _reader;

        private readonly CancellationTokenSource _readOperation;

        private ExceptionDispatchInfo _readException;

        #endregion

        #region Properties

        public long? Length { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     create a new pipe reader content.
        /// </summary>
        /// <param name="reader">the pipe reader that reads the content body.</param>
        /// <param name="length">the length of the body, or null if it is unknown.</param>
        /// <param name="cancellationToken">The token used to cancel the read operation.</param>
        public PipeReaderContent(PipeReader reader, long? length, CancellationToken cancellationToken = default)
        {
            (_reader, Length) = (reader, length);
            _readOperation = new CancellationTokenSource();
        }

        #endregion

        /// <summary>
        ///     sets an  exception that should be thrown when performing a read operation.
        /// </summary>
        /// <param name="readException">the exception that will be thrown from a read operation.</param>
        internal void SetReadException(ExceptionDispatchInfo readException)
        {
            _readException = readException;

            //cancel any ongoing read operations so that the exception can be thrown
            _readOperation.Cancel();
        }

        /// <summary>
        ///     Throws if a read exception has occured.
        /// </summary>
        private void ThrowIfNeeded() => _readException?.Throw();


        /// <summary>Serialize the HTTP content to a stream as an asynchronous operation.</summary>
        /// <param name="stream">The target stream.</param>
        /// <param name="context">
        ///     Information about the transport (channel binding token, for example). This parameter may be
        ///     <see langword="null" />.
        /// </param>
        /// <returns>The task object representing the asynchronous operation.</returns>
        protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            try
            {
                while (true)
                {
                    //wait for data to enter the pipe
                    ReadResult readResult = await _reader.ReadAsync(_readOperation.Token).ConfigureAwait(false);
                    ReadOnlySequence<byte> buffer = readResult.Buffer;

                    //if we have no more data to read, return
                    if (buffer.IsEmpty && readResult.IsCompleted)
                        return;

                    //write all the data to the stream
                    foreach (ReadOnlyMemory<byte> segment in buffer)
                    {
                        await stream.WriteAsync(segment, _readOperation.Token).ConfigureAwait(false);
                    }

                    //signal to the pipe we consumed the entire buffer
                    _reader.AdvanceTo(buffer.End);
                }
            }
            catch (OperationCanceledException)
            { } //catch our exception here so we could throw the original exception
            finally
            {
                //throw any read error that happen during the operation
                ThrowIfNeeded();
            }
        }

        /// <summary>Determines whether the HTTP content has a valid length in bytes.</summary>
        /// <param name="length">The length in bytes of the HTTP content.</param>
        /// <returns>
        ///     <see langword="true" /> if <paramref name="length" /> is a valid length; otherwise, <see langword="false" />.
        /// </returns>
        protected override bool TryComputeLength(out long length)
        {
            if (Length is null)
            {
                length = -1;
                return false;
            }

            length = Length.Value;
            return true;
        }

    }

}