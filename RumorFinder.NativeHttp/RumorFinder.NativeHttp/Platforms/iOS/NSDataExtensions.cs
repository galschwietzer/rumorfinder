﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using Foundation;

namespace RumorFinder.NativeHttp.Platforms.iOS
{

    // ReSharper disable once InconsistentNaming
    internal static class NSDataExtensions
    {

        /// <summary>
        ///     Writes the contents of the NSData to a buffer.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="buffer"></param>
        public static void WriteTo(this NSData data, Memory<byte> buffer)
        {
            byte[] internalBuffer;
            int bufferOffset;

            //try to write directly to the buffer first
            if (MemoryMarshal.TryGetArray(buffer, out ArraySegment<byte> array))
            {
                internalBuffer = array.Array;
                bufferOffset = array.Offset;

                //write the contents directly to the buffer
                data.EnumerateByteRange(WriteBlockToMemory);
                return;
            }

            //if we couldn't write directly we use a "proxy" buffer
            internalBuffer = new byte[data.Length];
            bufferOffset = 0;

            //write the contents to the internal buffer
            data.EnumerateByteRange(WriteBlockToMemory);

            //copy from internal buffer to supplied buffer
            new Span<byte>(internalBuffer).CopyTo(buffer.Span);
            return;

            void WriteBlockToMemory(IntPtr bytes, NSRange range, ref bool stop)
            {
                int blockPosition = (int) range.Location;
                int blockLength = (int) range.Length;

                //copy the current block to the internal buffer
                Marshal.Copy(bytes, internalBuffer, bufferOffset + blockPosition, blockLength);
            }
        }

    }

}