﻿using System;
using System.IO.Pipelines;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using RumorFinder.NativeHttp.Platforms.iOS;

// ReSharper disable once CheckNamespace
namespace RumorFinder.NativeHttp
{

    public partial class NativeMessageHandler
    {

        /// <summary>
        ///     represents a collection of data about an inflight request.
        /// </summary>
        private class InflightData
        {

            #region Properties

            public SemaphoreSlim Lock { get; } = new SemaphoreSlim(1, 1);
            public CancellationToken CancellationToken { get; set; }

            public Pipe ContentPipe { get; } = new Pipe();
            public long BytesWritten { get; set; } = 0;
            public float UploadProgress { get; set; } = 0;
            public IProgress<float> Progress { get; set; }
            public HttpRequestMessage RequestMessage { get; set; }
            public HttpResponseMessage ResponseMessage { get; set; }
            public PipeReaderContent ResponseContent { get; set; }
            public TaskCompletionSource<HttpResponseMessage> CompletionSource { get; set; }

            public bool ResponseSent { get; set; }
            public bool Errored { get; set; }
            public bool Completed { get; set; }
            public bool Done => Errored || Completed || CancellationToken.IsCancellationRequested;

            #endregion

        }

    }

}