﻿using UIKit;

namespace RumorFinder.Assets
{

    internal static class Colors
    {

        #region Properties

        public static UIColor HighlightedColor => UIColor.FromRGB(0, 172, 237);

        public static UIColor MainBackgroundColor => UIColor.FromRGB(20, 29, 38);

        public static UIColor SecondaryBackgroundColor => UIColor.FromRGB(36, 52, 71);

        public static UIColor MainTextColor => UIColor.White;

        public static UIColor SecondaryTextColor => UIColor.LightGray;

        public static UIColor Error => UIColor.Red;

        #endregion

    }

}