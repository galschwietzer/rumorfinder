﻿using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Assets
{

    internal static class Icons
    {

        #region Properties

        public static UIImage BackButton => UIImage.FromBundle("BackButton");

        public static UIImage DeleteButton => UIImage.FromBundle("DeleteButton");

        public static UIImage Clock => UIImage.FromBundle("ClockIcon");

        public static UIImage Recommendations => UIImage.FromBundle("Recommendations");

        public static UIImage FailedConnection => UIImage.FromBundle("FailedConnectionIcon");

        public static UIImage SearchUnsuccessful => UIImage.FromBundle("SearchFailedIcon");

        public static UIImage Home => UIImage.FromBundle("Home");

        public static UIImage HomeSelected => UIImage.FromBundle("Home-selected");

        public static UIImage Products => UIImage.FromBundle("Products");

        public static UIImage ProductsSelected => UIImage.FromBundle("Products-selected");

        public static UIImage Notification => UIImage.FromBundle("Notification");

        #endregion

        public static UIImage For(RumorCategory category) => UIImage.FromBundle($"{category.ToString()}Icon");

    }

}