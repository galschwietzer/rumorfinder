﻿using System.Collections.Generic;
using Foundation;
using UIKit;

namespace RumorFinder.Assets
{
    public class FollowButton
    {
        #region Static Fields and Constants

        private const string FollowPrompt = "    Follow    ";
        private const string FollowingMessage = "  Following  ";

        private static readonly Dictionary<UIControlState, UIColor> BackgroundColors =
            new Dictionary<UIControlState, UIColor>
            {
                {UIControlState.Normal, Colors.MainBackgroundColor},
                {UIControlState.Disabled, Colors.MainBackgroundColor},
                {UIControlState.Selected, Colors.HighlightedColor}
            };

        private static readonly Dictionary<UIControlState, UIColor> BorderColors =
            new Dictionary<UIControlState, UIColor>
            {
                {UIControlState.Normal, Colors.HighlightedColor}, {UIControlState.Disabled, UIColor.DarkGray},
                {UIControlState.Selected, Colors.HighlightedColor}
            };

        private static readonly Dictionary<UIControlState, string> Titles = new Dictionary<UIControlState, string>
        {
            {UIControlState.Normal, FollowButton.FollowPrompt}, {UIControlState.Disabled, FollowButton.FollowPrompt},
            {UIControlState.Selected, FollowButton.FollowingMessage}
        };

        private static readonly Dictionary<UIControlState, UIColor> TitleColors =
            new Dictionary<UIControlState, UIColor>
            {
                {UIControlState.Normal, Colors.HighlightedColor}, {UIControlState.Disabled, UIColor.DarkGray},
                {UIControlState.Selected, UIColor.White}
            };

        #endregion

        #region Properties

        public UIButton ButtonView { get; }

        public NSSet PossibleTitles => new NSSet(FollowButton.FollowPrompt, FollowButton.FollowingMessage);

        #endregion

        #region Constructors

        public FollowButton()
        {
            var button = new UIButton(UIButtonType.Custom) {AdjustsImageWhenHighlighted = false};

            button.Layer.BorderWidth = 0.2f;
            button.Layer.CornerRadius = 17;

            // initialize a feedback generator and have it
            // prepare to activate whenever there's a touch down event
            // so there will be no latency when the feedback goes off
            var feedbackGenerator = new UINotificationFeedbackGenerator();
            button.TouchDown += (sender, e) => feedbackGenerator.Prepare();
            button.TouchUpInside += (sender, e) =>
                feedbackGenerator.NotificationOccurred(UINotificationFeedbackType.Success);

            ButtonView = button;
            UpdateButtonForState(UIControlState.Normal);
        }

        #endregion

        /// <summary>
        ///     Updates the properties and size of the button view for the given state.
        /// </summary>
        /// <param name="state">
        ///     The state the button's appearance will now reflect.
        ///     Possible values are only Normal, Selected or Disabled.
        /// </param>
        public void UpdateButtonForState(UIControlState state)
        {
            ButtonView.SetTitle(FollowButton.Titles[state], UIControlState.Normal);
            ButtonView.SetTitleColor(FollowButton.TitleColors[state], UIControlState.Normal);
            ButtonView.BackgroundColor = FollowButton.BackgroundColors[state];
            ButtonView.Layer.BorderColor = FollowButton.BorderColors[state].CGColor;
            ButtonView.SizeToFit();
        }
    }
}