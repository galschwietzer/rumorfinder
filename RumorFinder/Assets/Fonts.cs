﻿using System;
using UIKit;

namespace RumorFinder.Assets
{

    internal static class Fonts
    {

        public static UIFont RobotoRegular(nfloat size) => UIFont.FromName("Roboto-Regular", size);

    }

}