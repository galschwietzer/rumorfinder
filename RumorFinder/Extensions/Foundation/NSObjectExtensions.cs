﻿using System;
using System.Threading.Tasks;
using Foundation;
using RumorFinder.Util;
using UIKit;

namespace RumorFinder.Extensions.Foundation
{

    internal static class NsObjectExtensions
    {

        /// <summary>
        ///     returns the current user data inside a User object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static User GetCurrentUser(this NSObject obj)
        {
            var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
            return appDelegate?.CurrentUser;
        }

        /// <summary>
        ///     method to invoke and register a background safe operation with the OS
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="operation"></param>
        public static void InvokeBackgroundSafeOperation(this NSObject obj, Action operation)
        {
            nint operationId = UIApplication.SharedApplication.BeginBackgroundTask(() => { });
            try
            {
                operation();
            }
            finally
            {
                UIApplication.SharedApplication.EndBackgroundTask(operationId);
            }
        }

        /// <summary>
        ///     method to invoke and register a background safe operation with the OS
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="operationSupplier">the function used to retrieve the operation in the from of a Task</param>
        /// <returns>A task that will be completed when the supplied task completes.</returns>
        public static async Task InvokeBackgroundSafeOperation(this NSObject obj, Func<Task> operationSupplier)
        {
            nint operationId = UIApplication.SharedApplication.BeginBackgroundTask(() => { });
            try
            {
                await operationSupplier();
            }
            finally
            {
                UIApplication.SharedApplication.EndBackgroundTask(operationId);
            }
        }

        /// <summary>
        ///     method to invoke and register a background safe operation with the OS
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="operationSupplier">the function used to retrieve the operation in the from of a Task</param>
        /// <returns>The value returned by the awaited task.</returns>
        public static async Task<T> InvokeBackgroundSafeOperation<T>(this NSObject obj, Func<Task<T>> operationSupplier)
        {
            nint operationId = UIApplication.SharedApplication.BeginBackgroundTask(() => { });
            try
            {
                return await operationSupplier();
            }
            finally
            {
                UIApplication.SharedApplication.EndBackgroundTask(operationId);
            }
        }

    }

}