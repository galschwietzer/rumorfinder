﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using RumorFinder.Foundation;

namespace RumorFinder.Extensions.RumorFinder.Foundation
{
    public static class ProductListExtensions
    {
        /// <summary>
        ///     Updates all the products in the list and sends local notification for each updated product
        /// </summary>
        /// <param name="productList">the list to perform the update for.</param>
        /// <param name="isUserInitiated">is the update was user initiated.</param>
        /// <param name="cancellationToken">the token used to cancel the operation.</param>
        /// <param name="progress">the object used to track the progress of the operation.</param>
        /// <returns>The status of the update operation.</returns>
        public static async Task<OperationStatus> UpdateAllProductsAndSendNotificationsAsync(
            this ProductList productList,
            bool isUserInitiated = true,
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null)
        {
            ConcurrentBag<Product> updatedProducts = new ConcurrentBag<Product>();

            // doing this so we know which products where updated
            foreach (Product product in productList)
            {
                product.RumorsChanged += ProductUpdated;
            }

            // attempt to fetch new data
            OperationStatus status = await productList
                                           .UpdateAllProductsAsync(isUserInitiated, cancellationToken, progress)
                                           .ConfigureAwait(false);

            // unsubscribe from the event 
            foreach (Product product in productList)
            {
                product.RumorsChanged -= ProductUpdated;
            }

            // send the notifications
            AppDelegate.SendNotificationsForUpdatedProducts(updatedProducts);

            return status;

            //-------------helper methods------------------
            // called when a product is updated to inform the product was updated
            void ProductUpdated(object sender, EventArgs e)
            {
                updatedProducts.Add((Product) sender);
            }
        }
    }
}