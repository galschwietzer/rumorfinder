﻿namespace RumorFinder.Extensions.System
{

    internal static class ObjectExtensions
    {

        /// <summary>
        ///     returns a one object array containing this object only
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T[] AsOneObjectArray<T>(this T obj) =>
            new[] {obj};

    }

}