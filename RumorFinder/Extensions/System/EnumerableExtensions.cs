﻿using System.Collections.Generic;
using System.Linq;
using Foundation;

namespace RumorFinder.Extensions.System
{

    internal static class EnumerableExtensions
    {

        /// <summary>
        ///     returns an array of the sequence's index paths
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="section">The section of the returned index paths</param>
        /// <returns></returns>
        public static NSIndexPath[] GetIndexPaths<T>(this IEnumerable<T> sequence, int section = 0)
        {
            int count = sequence.Count();
            var indexPaths = new NSIndexPath[count];
            for (int i = 0; i < count; i++)
            {
                var indexPath = NSIndexPath.FromRowSection(i, section);
                indexPaths[i] = indexPath;
            }

            return indexPaths;
        }

    }

}