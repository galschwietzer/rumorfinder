﻿using UIKit;

namespace RumorFinder.Extensions.UIKit
{

    internal static class UiViewExtensions
    {

        /// <summary>
        ///     removes all constraints affecting this view.
        /// </summary>
        /// <param name="view"></param>
        public static void RemoveAllConstraints(this UIView view)
        {
            var superview = view.Superview;
            while (superview != null)
            {
                foreach (var constraint in superview.Constraints)
                {
                    if (constraint.FirstItem.Equals(view) || constraint.SecondItem.Equals(view))
                        superview.RemoveConstraint(constraint);
                }

                superview = superview.Superview;
            }

            view.RemoveConstraints(view.Constraints);
        }

    }

}