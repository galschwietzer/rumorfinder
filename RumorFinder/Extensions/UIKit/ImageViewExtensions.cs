﻿using CoreGraphics;
using UIKit;

namespace RumorFinder.Extensions.UIKit
{

    internal static class ImageViewExtensions
    {

        /// <summary>
        ///     sets the supplied image as the view's image and resizes its frame accordingly
        /// </summary>
        /// <param name="view"></param>
        /// <param name="image"></param>
        public static void SetImage(this UIImageView view, UIImage image)
        {
            view.Image = image;
            view.Frame = new CGRect(view.Frame.X, view.Frame.Y, image.Size.Width, image.Size.Height);
        }

    }

}