﻿using Foundation;
using RumorFinder.Assets;
using UIKit;

namespace RumorFinder.Extensions.Assets
{

    internal static class StoryboardsExtensions
    {

        /// <summary>
        ///     Instantiates a view controller from the storyboard associated with the supplied enum
        /// </summary>
        /// <typeparam name="TViewController"></typeparam>
        /// <param name="storyboardName"></param>
        /// <returns></returns>
        public static TViewController InstantiateViewController<TViewController>(this Storyboards storyboardName)
            where TViewController : UIViewController
        {
            UIStoryboard storyboard = UIStoryboard.FromName(storyboardName.ToString(), NSBundle.MainBundle);
            return storyboard.InstantiateViewController(typeof(TViewController).Name) as TViewController;
        }

    }

}