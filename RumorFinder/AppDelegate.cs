﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using RumorFinder.Assets;
using RumorFinder.Extensions.RumorFinder.Foundation;
using RumorFinder.Foundation;
using RumorFinder.Util;
using UIKit;
using UserNotifications;

namespace RumorFinder
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        #region Static Fields and Constants

        // this indicates 6 hours in seconds
        private const double MinimumFetchInterval = 60 * 60 * 6;

        private const string NotificationTitle = "Found New Rumors!";
        private const string NotificationBodyFormat = "New rumors were found for {0}!";
        private const double NotificationTriggerTimeInterval = 5;

        #endregion

        #region Properties

        public User CurrentUser { get; private set; }

        // class-level declarations

        public override UIWindow Window { get; set; }

        #endregion

        /// <summary>
        ///     Sends a local notification for each of the updated products
        /// </summary>
        /// <param name="updatedProducts">The products to send notifications about.</param>
        public static void SendNotificationsForUpdatedProducts(IEnumerable<Product> updatedProducts)
        {
            if (!updatedProducts.Any())
            {
                return;
            }
            // send notifications foreach of the updated products (if notifications are allowed
            UNUserNotificationCenter.Current.GetNotificationSettings(settings =>
            {
                bool alertsEnabled = settings.AlertSetting is UNNotificationSetting.Enabled;
                bool badgeEnabled = settings.BadgeSetting is UNNotificationSetting.Enabled;

                if (settings.AuthorizationStatus != UNAuthorizationStatus.Authorized ||
                    !(alertsEnabled || badgeEnabled))
                {
                    // this means we can't display notifications or can't display the kind we want to
                    return;
                }

                // send notifications regarding all of the updated products
                foreach (Product updatedProduct in updatedProducts)
                {
                    // create a personalized request for each product
                    var request = CreateNotificationForUpdatedProduct(updatedProduct, alertsEnabled, badgeEnabled);

                    // send the request
                    UNUserNotificationCenter.Current.AddNotificationRequest(request, null);
                }
            });

            // create a notification stating the product was updated
            UNNotificationRequest CreateNotificationForUpdatedProduct(
                Product updatedProduct,
                bool alertsEnabled,
                bool badgeEnabled)
            {
                var content = new UNMutableNotificationContent();
                if (alertsEnabled)
                {
                    content.Title = AppDelegate.NotificationTitle;
                    content.Body = string.Format(AppDelegate.NotificationBodyFormat, updatedProduct.Name);
                }

                if (badgeEnabled) content.Badge = 1;

                var trigger =
                    UNTimeIntervalNotificationTrigger.CreateTrigger(AppDelegate.NotificationTriggerTimeInterval, false);

                return UNNotificationRequest.FromIdentifier(updatedProduct.Name, content, trigger);
            }
        }

        /// <param name="application">Handle to the UIApplication.</param>
        /// <param name="completionHandler">
        ///     Callback to invoke to notify the operating system of the result of the background fetch
        ///     operation.
        /// </param>
        /// <summary>Background support: Invoked by the operating system to allow an application to download data.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is part of iOS 7.0 new background fetch
        ///         support.  This method is invoked if your Entitlements list
        ///         the "fetch" background operation and after you have
        ///         enabled fetching by calling the <see cref="M:UIKit.UIApplication.SetMinimumBackgroundFetchInterval" />
        ///         method.
        ///     </para>
        ///     <para>
        ///         Once that happens the operating system will determine the
        ///         appropriate time to wake up your application to allow it
        ///         to download data.  When it does that, it will first call
        ///         the <see cref="M:UIKit.UIApplicationDelegate.FinishedLaunching" />
        ///         method and then will invoke this method.
        ///     </para>
        ///     <para>
        ///         This method should download the data from the network and
        ///         before it completes, it must invoke the provided callback
        ///         with the appropriate status code to notify the operating
        ///         system of the background fetch operation (new data was
        ///         downloaded, there was a network connection problem or no
        ///         new data was found).
        ///     </para>
        ///     <para>
        ///         Upon completion, you must notify the operating system of the result of the data transfer by invoking the
        ///         provided callback.
        ///     </para>
        ///     <para>
        ///         Important: failure to call the provided callback method
        ///         with the result code before this method completes will
        ///         cause your application to be terminated.
        ///     </para>
        /// </remarks>
        public override async void PerformFetch(
            UIApplication application,
            Action<UIBackgroundFetchResult> completionHandler)
        {
            var productList = CurrentUser.FollowedProducts;

            // attempt to fetch new data
            OperationStatus status = await productList.UpdateAllProductsAndSendNotificationsAsync(false);

            UIBackgroundFetchResult result = UIBackgroundFetchResult.Failed;
            switch (status)
            {
                case OperationStatus.Failed:
                    result = UIBackgroundFetchResult.Failed;
                    break;
                case OperationStatus.Unsuccessful:
                    result = UIBackgroundFetchResult.NoData;
                    break;
                case OperationStatus.Successful:
                    result = UIBackgroundFetchResult.NewData;
                    break;
            }

            completionHandler(result);
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            // request the user's permission to use notifications
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert |
                                                                  UNAuthorizationOptions.Badge,
                                                                  delegate { });

            //deserialize the user data from file
            CurrentUser = User.FromFile();

            // enable Background app refresh for a maximum of 4 times a day (interval is 6 hours)
            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(AppDelegate.MinimumFetchInterval);

            //set the default color of navigation bars
            UINavigationBar.Appearance.BarTintColor = Colors.SecondaryBackgroundColor;
            UINavigationBar.Appearance.TintColor = UIColor.White;

            //set the default text attributes
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes
            {
                TextColor = Colors.MainTextColor,
                TextShadowColor = UIColor.Clear,
                Font = UIFont.SystemFontOfSize(20f)
            });

            UINavigationBar.Appearance.Translucent = false;

            //set the large title's default text colors
            UINavigationBar.Appearance.LargeTitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = Colors.MainTextColor
            };


            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message)
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background execution this method is called instead of WillTerminate when the user quits.

            //write the user data to file
            CurrentUser.WriteToFile();
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transition from background to active state.
            // Here you can undo many of the changes made on entering the background.

            //deserialize the user data from file
            CurrentUser = User.FromFile();
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive.
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
    }
}