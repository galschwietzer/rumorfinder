﻿using System;
using System.Collections.Immutable;
using RumorFinder.Foundation;
using RumorFinder.Foundation.Text;
using RumorFinder.NaturalLanguage;
using UIKit;

namespace RumorFinder
{

    public class Application
    {

        #region Constructors

        static Application()
        {
            //only use the NaturalLanguage Framework if the device is iOS 12 and above
            if (UIDevice.CurrentDevice.CheckSystemVersion(12, 0))
            {
                //compile the NLModels
                NaturalLanguageClassifier.ForceCompile();

                // set the classification method to the NaturalLanguageClassifier
                Classifier.SetHeadlineClassificationMethod(NaturalLanguageClassifier.ClassifyHeadline, false);
                Classifier.SetRumorClassificationMethod(NaturalLanguageClassifier.ClassifyRumor, false);
            }

            // forbes always gives bad results
            Product.BlackListedSources = ImmutableList<Uri>.Empty.Add(new Uri("https://www.forbes.com/"));
        }

        #endregion

        // This is the main entry point of the application.
        private static void Main(string[] args)
        {
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            try
            {
                UIApplication.Main(args, null, "AppDelegate");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                throw;
            }
        }

    }

}