﻿using System;
using System.Collections.Generic;
using CoreAnimation;
using CoreGraphics;
using Facebook.Pop;
using Foundation;
using RumorFinder.Extensions.System;
using UIKit;

namespace RumorFinder.Animations
{

    internal partial class AnimatedExpandingTransition : UIViewControllerAnimatedTransitioning
    {

        #region Static Fields and Constants

        private const string AnimationKeyPath = "path";

        private const int FrameCount = 120;

        private const int AnimationSpringBounciness = 25;
        private const float AnimationVelocity = 100;
        private static readonly nfloat AnimationDelta = 1f;

        #endregion

        #region Fields

        private readonly double _animationDuration;

        private readonly bool _isPresenting;
        private readonly UIView _originView;
        private readonly CGRect _originFrame;

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructs a new animated transition with the given parameters
        /// </summary>
        /// <param name="originView">The origin view for this animation</param>
        /// <param name="originFrame">The origin frame for origin view.
        /// This field can be used in case the frame of the origin view needs to be converted between views.</param>
        /// <param name="animationDuration">The duration of the transition.</param>
        /// <param name="isPresenting">Whether the view being pushed(presented) or popped(dismissed).</param>
        public AnimatedExpandingTransition(
            UIView originView,
            CGRect originFrame,
            double animationDuration,
            bool isPresenting) =>
            (_originView, _originFrame, _animationDuration, _isPresenting) =
            (originView, originFrame ,animationDuration, isPresenting);

        #endregion

        /// <summary>Returns the duration, in seconds, of the transition animation.</summary>
        /// <param name="transitionContext">The context object containing information to use during the transition.</param>
        /// <returns>The duration, in seconds, of the custom transition animation.</returns>
        public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext) =>
            _animationDuration;

        /// <summary>Performs a custom UIViewController transition animation.</summary>
        /// <param name="transitionContext">The context object containing information to use during the transition.</param>
        public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
        {
            // get the needed views for the animations
            UIView containerView = transitionContext.ContainerView;

            UIView fromView = transitionContext.GetViewFor(UITransitionContext.FromViewKey);
            UIView toView = transitionContext.GetViewFor(UITransitionContext.ToViewKey);

            UIView animatedView = _isPresenting ? toView : fromView;

            // add the subviews
            if (toView != null) containerView.Add(toView);
            containerView.BringSubviewToFront(animatedView);

            // this is the frame that represents the end state the presented view
            CGRect completeFrame =
                transitionContext.GetFinalFrameForViewController(transitionContext
                                                                     .GetViewControllerForKey(UITransitionContext
                                                                                                  .ToViewControllerKey));

            // assign the starting values
            CGRect startingFrame = _originFrame;
            nfloat startingRadius = _originView.Layer.CornerRadius;

            // create the mask that will be animated
            var startingMask = new CAShapeLayer
            {
                Path = _isPresenting
                    ? UIBezierPath.FromRoundedRect(startingFrame,
                                                   UIRectCorner.AllCorners,
                                                   new CGSize(startingRadius, startingRadius)).CGPath
                    : UIBezierPath.FromRect(completeFrame).CGPath
            };

            // add the mask to the container view
            containerView.Layer.Mask = startingMask;
            containerView.ClipsToBounds = true;

            // create the animation
            var animation = CAKeyFrameAnimation.FromKeyPath(AnimatedExpandingTransition.AnimationKeyPath);
            animation.TimingFunctions = CAMediaTimingFunction.FromName(CAMediaTimingFunction.EaseIn).AsOneObjectArray();
            animation.Values = PathsForAnimation();
            animation.Duration = _animationDuration;
            animation.RemovedOnCompletion = false;
            animation.FillMode = CAFillMode.Forwards;
            animation.Delegate = new AnimationDelegate(Completion);

            // run the animation
            containerView.Layer.Mask.AddAnimation(animation, nameof(animation));

            // this is the animation completion action
            void Completion(bool animationRanToCompletion)
            {
                if (animationRanToCompletion)
                {
                    // we do this to avoid visual bugs with the bounce animation
                    fromView?.RemoveFromSuperview();

                    // execute a vertical bounce animation for added effect
                    // if we perform the animation we call the complete transition method 
                    // only after the bounce animation ends
                    UIView bouncedView = _isPresenting ? containerView : _originView;
                    BounceViewVertically(bouncedView, () => transitionContext.CompleteTransition(true));
                }
                else
                    transitionContext.CompleteTransition(false);
            }

            //----------------Helper Methods----------------

            // returns the array of path values for the animation
            NSObject[] PathsForAnimation()
            {
                var paths = new List<NSObject>();

                // populate the list of paths
                for (float fractionComplete = 0;
                     fractionComplete <= 1f;
                     fractionComplete += 1f / AnimatedExpandingTransition.FrameCount)
                {
                    paths.Add(PathForFractionComplete(fractionComplete));
                }

                // the order for the array is for presenting animation
                // so we reverse it if we are not presenting
                if (!_isPresenting)
                    paths.Reverse();

                return paths.ToArray();

                // creates a path from the given fraction completed of the animation
                NSObject PathForFractionComplete(nfloat fractionComplete)
                {
                    // the radius starts at the highest value and descends to 0
                    nfloat newRadius = startingRadius * (1 - fractionComplete);

                    nfloat newX = startingFrame.X * (1 - fractionComplete);
                    nfloat newY = startingFrame.Y * (1 - fractionComplete);
                    nfloat newWidth = startingFrame.Width +
                                      (completeFrame.Width - startingFrame.Width) * fractionComplete;
                    nfloat newHeight = startingFrame.Height +
                                       (completeFrame.Height - startingFrame.Height) * fractionComplete;

                    var newFrame = new CGRect(newX, newY, newWidth, newHeight);

                    // create the path from the new values
                    var path = UIBezierPath
                               .FromRoundedRect(newFrame, UIRectCorner.AllCorners, new CGSize(newRadius, newRadius))
                               .CGPath;

                    return FromObject(path);
                }
            }
        }

        #region Private Helpers

        /// <summary>
        ///     Executes a vertical bounce animation for the given view
        ///     and calls the completion action when it ends
        /// </summary>
        /// <param name="view"></param>
        /// <param name="completion"></param>
        private void BounceViewVertically(UIView view, Action completion)
        {
            // create the animation
            POPSpringAnimation bounceAnimation =
                POPSpringAnimation.AnimationWithPropertyNamed(POPAnimation.LayerPositionY);

            // we set the ToValue to the same value but with a really small delta because
            // of a bug in Facebook POP animations
            CGPoint currentPosition = view.Layer.Position;
            bounceAnimation.ToValue =
                NSNumber.FromNFloat(currentPosition.Y + AnimatedExpandingTransition.AnimationDelta);

            // configure its
            bounceAnimation.SpringBounciness = AnimatedExpandingTransition.AnimationSpringBounciness;
            bounceAnimation.Velocity = (NSNumber) AnimatedExpandingTransition.AnimationVelocity;
            bounceAnimation.CompletionAction = (animation, b) =>
            {
                // reset the view back to it's position
                view.Layer.Position = currentPosition;

                // call the given completion
                completion();
            };

            // execute it
            view.Layer.AddAnimation(bounceAnimation, nameof(bounceAnimation));
        }

        #endregion

    }

}