﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreML;
using Foundation;
using NaturalLanguage;
using RumorFinder.Foundation;

namespace RumorFinder.NaturalLanguage
{

    /// <summary>
    ///     Represents a NL classifier that leverages CoreML and the NaturalLanguage Framework.
    /// </summary>
    public static class NaturalLanguageClassifier
    {

        #region Static Fields and Constants

        private const string ProductNameIdentifier = " productname ";

        private static readonly NLModel CategoryClassifier;
        private static readonly NLModel HeadlineClassifier;

        /// <summary>
        ///     The table used to convert between the NLModel's output and a HeadlineCategory object,
        /// </summary>
        private static readonly Dictionary<string, HeadlineCategory> HeadlineCategoryConversionTable =
            new Dictionary<string, HeadlineCategory>
            {
                ["rumor"] = HeadlineCategory.Rumor,
                ["other"] = HeadlineCategory.Other
            };

        /// <summary>
        ///     The table used to convert between the NLModel's output and a RumorCategory object,
        /// </summary>
        private static readonly Dictionary<string, RumorCategory> RumorCategoryConversionTable =
            new Dictionary<string, RumorCategory>
            {
                ["specification"] = RumorCategory.Specifications,
                ["non rumor"] = RumorCategory.NonRumor,
                ["release date"] = RumorCategory.ReleaseDate,
                ["price"] = RumorCategory.Price,
                ["features"] = RumorCategory.Features,
                ["design"] = RumorCategory.Design,
                ["other"] = RumorCategory.Other
            };

        #endregion

        #region Constructors

        /// <summary>
        ///     used to initialize the NLModel.
        /// </summary>
        static NaturalLanguageClassifier()
        {
            NaturalLanguageClassifier.HeadlineClassifier =
                CompileModel(nameof(NaturalLanguageClassifier.HeadlineClassifier));

            NaturalLanguageClassifier.CategoryClassifier =
                CompileModel(nameof(NaturalLanguageClassifier.CategoryClassifier));

            // helper method to compile a .mlmodel file into a NLModel
            NLModel CompileModel(string fileName)
            {
                // retrieve the model url and compile it
                NSUrl modelUrl =
                    NSBundle.MainBundle.GetUrlForResource(fileName,
                                                          "mlmodel");
                NSUrl compiledUrl = MLModel.CompileModel(modelUrl, out _);

                // find the app support directory
                var fileManager = NSFileManager.DefaultManager;
                NSUrl appSupportDirectory = fileManager.GetUrl(NSSearchPathDirectory.ApplicationSupportDirectory,
                                                               NSSearchPathDomain.User,
                                                               compiledUrl,
                                                               true,
                                                               out _);

                // create a permanent URL in the app support directory
                NSUrl permanentUrl = appSupportDirectory.Append(compiledUrl.LastPathComponent, false);

                // if the file exists, replace it. Otherwise, copy the file to the destination.
                if (fileManager.FileExists(permanentUrl.AbsoluteString))
                {
                    _ = fileManager.Replace(permanentUrl,
                                            compiledUrl,
                                            string.Empty,
                                            NSFileManagerItemReplacementOptions.UsingNewMetadataOnly,
                                            out _,
                                            out _);
                }
                else
                    fileManager.Copy(compiledUrl, permanentUrl, out _);

                // create the NLModel from the compiled model
                return NLModel.Create(permanentUrl, out _);
            }
        }

        #endregion

        /// <summary>
        ///     Forces the static constructor to run, which in turn will force the compile operation to occur.
        /// </summary>
        public static void ForceCompile()
        { }

        /// <summary>
        ///     classifies an online headline
        /// </summary>
        /// <param name="headline">headline to classify</param>
        /// <param name="productName">the product name associated to the headline</param>
        /// <returns>the category of the given headline</returns>
        public static HeadlineCategory ClassifyHeadline(string headline, string productName)
        {
            if (string.IsNullOrEmpty(headline) ||
                headline.IndexOf(productName, StringComparison.CurrentCultureIgnoreCase) < 0)
                return HeadlineCategory.Other;

            // replace the product name with the product name identifier(which is the expected input for the model)
            string replacedText = Regex.Replace(headline, productName, NaturalLanguageClassifier.ProductNameIdentifier, RegexOptions.IgnoreCase);

            string prediction = NaturalLanguageClassifier.HeadlineClassifier.GetPredictedLabel(replacedText);

            return NaturalLanguageClassifier.HeadlineCategoryConversionTable[prediction];
        }
            

        /// <summary>
        ///     classifies a paragraph from an online rumor article using a trained NLModel.
        /// </summary>
        /// <param name="rumor">the rumor paragraph to classify</param>
        /// <param name="productName">the product name associated to the online article</param>
        /// <returns>the category of the given rumor paragraph</returns>
        public static RumorCategory ClassifyRumor(string rumor, string productName)
        {
            // replace the product name with the product name identifier(which is the expected input for the model)
            string replacedText = Regex.Replace(rumor, productName, NaturalLanguageClassifier.ProductNameIdentifier, RegexOptions.IgnoreCase);

            // predict the category
            string prediction = NaturalLanguageClassifier.CategoryClassifier.GetPredictedLabel(replacedText);

            // convert prediction to enum value
            return NaturalLanguageClassifier.RumorCategoryConversionTable[prediction];
        }

    }

}