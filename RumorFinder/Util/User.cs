﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using Newtonsoft.Json;
using RumorFinder.Foundation;
using RumorFinder.Foundation.Recommendations;

namespace RumorFinder.Util
{

    /// <summary>
    ///     Represents an application user
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public partial class User : IAppUser
    {

        #region Static Fields and Constants

        private const int MaximumRecentSearchesCount = 9;

        #endregion

        #region Fields

        [JsonProperty("recentSearches")]
        private readonly List<Product> _recentSearches;

        [JsonProperty("declinedRecommendations")]
        private readonly HashSet<Product> _declinedRecommendations;

        [JsonProperty("recommendations")]
        private ImmutableList<Product> _recommendations;

        #endregion

        #region Properties

        public static string UserDataFilePath
        {
            get
            {
                string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string library = Path.Combine(documents, "..", "Library");
                string filename = Path.Combine(library, "UserData.json");
                return filename;
            }
        }

        public IReadOnlyList<Product> RecentSearches => _recentSearches;

        [JsonProperty("followedProducts")]
        public ProductList FollowedProducts { get; }

        public IReadOnlyList<Product> Recommendations => _recommendations;

        #endregion

        #region Constructors

        public User() : this(string.Empty, new List<Product>(), new ProductList(new FollowedProductsComparer()), new HashSet<Product>(), ImmutableList<Product>.Empty)
        { }

        [JsonConstructor]
        private User(string id, List<Product> recentSearches, ProductList followedProducts, HashSet<Product> declinedRecommendations, ImmutableList<Product> recommendations)
        {
            if (id == string.Empty) id = CreateID(); 
            if (followedProducts is null) followedProducts = new ProductList(new FollowedProductsComparer());

            // we want to make sure the followed products comparer is correct
            if (followedProducts.Comparer.GetType() != typeof(FollowedProductsComparer))
                followedProducts = new ProductList(followedProducts, new FollowedProductsComparer());

            // make sure we listen to the collection changes event
            followedProducts.CollectionChanged -= OnFollowedProductsChanged;
            followedProducts.CollectionChanged += OnFollowedProductsChanged;

            ID = id;
            _recentSearches = recentSearches;
            FollowedProducts = followedProducts;
            _recommendations = recommendations;
            _declinedRecommendations = declinedRecommendations;
        }

        #endregion

        #region IAppUser Properties And Methods

        /// <summary>
        ///     Creates a version 4 UUID
        /// </summary>
        /// <returns></returns>
        private static string CreateID() => new NSUuid().AsString();

        [JsonProperty("id")]
        public string ID { get; }

        IEnumerable<Product> IAppUser.FollowedProducts => FollowedProducts;

        IEnumerable<Product> IAppUser.SearchedProducts => RecentSearches;

        IEnumerable<Product> IAppUser.DeclinedRecommendations => _declinedRecommendations;

        #endregion

        /// <summary>
        ///     deserializes a user object from file.
        /// </summary>
        /// <param name="filePath">the path to the json file containing the deserialized user object</param>
        /// <returns>the deserialized user object, or a new object if the file doesn't exists or is empty</returns>
        public static User FromFile(string filePath = null)
        {
            filePath = filePath ?? User.UserDataFilePath;

            string data = string.Empty;

            if (File.Exists(filePath))
                data = File.ReadAllText(filePath);

            if (string.IsNullOrEmpty(data))
                return new User();

            try
            {
                return JsonConvert.DeserializeObject<User>(data,
                                                           new JsonSerializerSettings
                                                               {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            }
            catch (JsonException)
            {
                return new User();
            }
        }

        /// <summary>
        ///     deserializes the object to a txt file
        /// </summary>
        /// <param name="filePath">the path to the json file containing the deserialized user object</param>
        public void WriteToFile(string filePath = null)
        {
            filePath = filePath ?? User.UserDataFilePath;

            try
            {
                string data = JsonConvert.SerializeObject(this,
                                                          Formatting.None,
                                                          new JsonSerializerSettings
                                                              {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
                File.WriteAllText(filePath, data);
            }
            catch (JsonException)
            { }
            catch (IOException)
            { }
        }

        /// <summary>
        ///     adds a new recent search and makes sure the size doesn't exceeds the maximum and saves the changes to file.
        /// </summary>
        /// <param name="recentSearch">
        ///     the recent search made by the user. if the user already searched this item, the old item is
        ///     assigned to the variable and the old cell is just moved to the top
        /// </param>
        /// <param name="oldIndex">the old index of the recent search if it was already in the list, -1 otherwise.</param>
        /// <param name="newIndex">the new index of the recent search.</param>
        /// <param name="exceededMaximum">
        ///     whether or not the list exceeded it's maximum length with this new addition.
        ///     if that's the case, the recent search at the bottom of the list will be removed/
        /// </param>
        public void AddRecentSearch(
            ref Product recentSearch,
            out int oldIndex,
            out int newIndex,
            out bool exceededMaximum)
        {
            if (_recentSearches.Contains(recentSearch))
            {
                oldIndex = _recentSearches.IndexOf(recentSearch);

                // if item already exists reassign the old item to the variable
                recentSearch = RecentSearches[oldIndex];

                _recentSearches.RemoveAt(oldIndex);
                _recentSearches.Insert(0, recentSearch);
            }
            else
            {
                oldIndex = -1;
                _recentSearches.Insert(0, recentSearch);
            }

            newIndex = 0;

            // if there are too many recent searches we remove the last one
            exceededMaximum = RecentSearches.Count > User.MaximumRecentSearchesCount;
            if (exceededMaximum)
                _recentSearches.RemoveAt(RecentSearches.Count - 1);

            // save the changes
            WriteToFile();
        }

        /// <summary>
        ///     Returns the index of the given recent search.
        /// </summary>
        /// <param name="recentSearch"></param>
        /// <returns></returns>
        public int IndexOfRecentSearch(Product recentSearch) => _recentSearches.IndexOf(recentSearch);

        /// <summary>
        ///     Removes the recent search entry at the given index and saves the changes.
        /// </summary>
        /// <param name="index"></param>
        public void RemoveRecentSearchAt(int index)
        {
            if (index < 0)
                return;

            _recentSearches.RemoveAt(index);

            // save the changes
            WriteToFile();
        }

        /// <summary>
        ///     Removes all entries from the recent search list adn saves the data.
        /// </summary>
        public void ClearSearchHistory()
        {
            _recentSearches.Clear();

            // save the changes
            WriteToFile();
        }

        /// <summary>
        ///     returns whether this user follows the given product.
        /// </summary>
        /// <param name="product">The product to check if the user follows.</param>
        /// <returns>true if the user follows the product, false otherwise</returns>
        public bool Follows(Product product) => FollowedProducts.Contains(product);

        /// <summary>
        ///     Follows the given product and saves the changes to file.
        /// </summary>
        /// <param name="product">The product to follow.</param>
        public void Follow(Product product) => FollowedProducts.Add(product);

        /// <summary>
        ///     Un-follows the given product and saves the changes to file.
        /// </summary>
        /// <param name="followedProduct">the product to un-follow.</param>
        public void UnFollow(Product followedProduct) => FollowedProducts.Remove(followedProduct);

        /// <summary>
        ///     Refreshes the recommendations list for this user.
        ///     New recommendations will be added to the top of the list.
        /// </summary>
        /// <param name="cancellationToken">The token used to cancel this operation.</param>
        /// <param name="progress">The object used to track the progress of the operation.</param>
        /// <returns>The status of the operation.</returns>
        public async Task<OperationStatus> RefreshRecommendationsAsync(
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null)
        {
            // any recommendations that are still in the list are 
            // considered declined recommendations
            foreach (Product recommendation in _recommendations)
            {
                _declinedRecommendations.Add(recommendation);
            }

            IEnumerable<Product> recommendations;
            try
            {
                recommendations = await Recommender.GetRecommendationsAsync(this, cancellationToken, progress)
                                                   .ConfigureAwait(false);

                if (!recommendations.Any())
                {
                    return OperationStatus.Unsuccessful;
                }
            }
            catch (RecommenderException)
            {
                return OperationStatus.Failed;
            }

            var newRecommendationsList = ImmutableList<Product>.Empty.AddRange(recommendations);

            // update the recommendations value and save the changes
            Interlocked.Exchange(ref _recommendations, newRecommendationsList);
            WriteToFile();

            return OperationStatus.Successful;
        }

        #region Private Helpers

        /// <summary>
        ///     Event listener for when the Followed products list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFollowedProductsChanged(object sender, EventArgs e) => WriteToFile();

        #endregion

    }

}