﻿using System;
using System.Collections.Generic;
using Foundation;
using RumorFinder.Assets;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Extensions.System;
using RumorFinder.Extensions.UIKit;
using RumorFinder.Foundation;
using RumorFinder.Util;
using UIKit;

namespace RumorFinder.Scenes.HomeView
{

    /// <summary>
    ///     represents a cell of a recent user search
    /// </summary>
    public partial class SuggestionTableCell : UITableViewCell
    {

        #region Fields

        private bool _didSetImages;

        #endregion

        #region Properties

        public Product DisplayedProduct { get; private set; }

        public UITableView TableView { get; set; }

        #endregion

        #region Constructors

        public SuggestionTableCell(IntPtr handle) : base(handle)
        { }

        #endregion

        public void SetDisplayedProduct(Product p, Suggestion suggestion)
        {
            // initialize the images
            if (!_didSetImages)
            {
                DeleteButton.SetImage(Icons.DeleteButton);
                _didSetImages = true;
            }

            // update the cell icon and delete button
            // (we only display the delete button if its a recent search)
            SuggestionIcon.SetImage(GetIconForSuggestion(suggestion));
            bool showButton = suggestion is Suggestion.RecentSearch;
            DeleteButton.Hidden = !showButton;
            DeleteButton.Enabled = showButton;

            // remove the previous delegate
            DeleteButton.TouchUpInside -= DeleteRecentSearch;

            Name.Text = p.Name;

            DisplayedProduct = p;

            DeleteButton.TouchUpInside += DeleteRecentSearch;
        }

        private UIImage GetIconForSuggestion(Suggestion suggestion)
        {
            return new Dictionary<Suggestion, UIImage>
            {
                [Suggestion.RecentSearch] = Icons.Clock,
                [Suggestion.Recommendation] = Icons.Recommendations
            }[suggestion];
        }

        /// <summary>
        ///     called when the delete button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteRecentSearch(object sender, EventArgs e)
        {
            int section = Array.IndexOf(SearchViewController.SupportedSuggestions, Suggestion.RecentSearch);
            User currentUser = this.GetCurrentUser();
            int index = currentUser.IndexOfRecentSearch(DisplayedProduct);
            currentUser.RemoveRecentSearchAt(index);
            var indexPath = NSIndexPath.FromRowSection(index, section);
            TableView.DeleteRows(indexPath.AsOneObjectArray(), UITableViewRowAnimation.Fade);
        }

    }

}