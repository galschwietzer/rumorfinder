﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using RumorFinder.Assets;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Scenes.HomeView
{
    public partial class SearchViewController
    {
        private sealed class SuggestionsTableSource : UITableViewSource
        {
            #region Static Fields and Constants

            private const double HeaderSpacing = 10;

            private static readonly nfloat HeaderHeight = 44;
            private static readonly UIFont HeaderFont = UIFont.SystemFontOfSize(18);
            private static readonly UIColor HeaderLabelTextColor = UIColor.LightGray;

            private static readonly Dictionary<Suggestion, UIColor> HeaderButtonColor =
                new Dictionary<Suggestion, UIColor>
                {
                    [Suggestion.RecentSearch] = UIColor.Red,
                    [Suggestion.Recommendation] = Colors.HighlightedColor
                };

            private static readonly Dictionary<Suggestion, string> HeaderLabelText = new Dictionary<Suggestion, string>
            {
                [Suggestion.RecentSearch] = "Recent Searches",
                [Suggestion.Recommendation] = "Recommendations"
            };

            private static readonly Dictionary<Suggestion, string> HeaderButtonText = new Dictionary<Suggestion, string>
            {
                [Suggestion.RecentSearch] = "Clear All",
                [Suggestion.Recommendation] = "Refresh"
            };

            private static readonly string CellIdentifier = typeof(SuggestionTableCell).Name;

            #endregion

            #region Properties

            public SearchViewController Superview { private get; set; }

            #endregion

            /// <param name="tableView">Table view requesting the cell.</param>
            /// <param name="indexPath">Location of the row where the cell will be displayed.</param>
            /// <summary>Called by the table view to get populate the row at <paramref name="indexPath" /> with a cell view.</summary>
            /// <returns>
            ///     An object that inherits from <see cref="T:UIKit.UITableViewCell" /> that the table can use for the specified
            ///     row. Do not return <see langword="null" /> or an assertion will be raised.
            /// </returns>
            /// <remarks>
            ///     <para>
            ///         This method is called once for each row that is visible on screen. During scrolling, it is called additional
            ///         times as new rows come into view. Cells that disappear from view are cached by the table view. The
            ///         implementation of this method should call the table view's
            ///         <see cref="M:UIKit.UITableView.DequeueReusableCell(Foundation.NSString)" /> method to obtain a cached cell
            ///         object for reuse (if <see langword="null" /> is returned, create a new cell instance). Be sure to reset all
            ///         properties of a reused cell.
            ///     </para>
            ///     <para>Declared in [UITableViewDataSource]</para>
            /// </remarks>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var cell =
                    tableView.DequeueReusableCell(SuggestionsTableSource.CellIdentifier) as SuggestionTableCell;

                // get the correct product list
                Suggestion suggestion = SearchViewController.SupportedSuggestions[indexPath.Section];
                IReadOnlyList<Product> products = GetProductList(suggestion);

                Product product = products[indexPath.Row];

                cell.TableView = tableView;
                cell.SetDisplayedProduct(product, suggestion);

                return cell;
            }

            /// <param name="tableview">Table view displaying the rows.</param>
            /// <param name="section">Index of the section containing the rows.</param>
            /// <summary>
            ///     Called by the table view to find out how many rows are to be rendered in the section specified by
            ///     <paramref name="section" />.
            /// </summary>
            /// <returns>Number of rows in the section at index <paramref name="section" />.</returns>
            /// <remarks>
            ///     <para>Declared in [UITableViewDataSource]</para>
            /// </remarks>
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                // get the correct product list
                Suggestion suggestion = SearchViewController.SupportedSuggestions[section];
                IReadOnlyList<Product> products = GetProductList(suggestion);
                return products.Count;
            }

            /// <param name="tableView">Table view displaying the sections.</param>
            /// <summary>Returns the number of sections that are required to display the data.</summary>
            /// <returns>Number of sections required to display the data. The default is 1 (a table must have at least one section).</returns>
            /// <remarks>
            ///     <para>Declared in [UITableViewDataSource]</para>
            /// </remarks>
            public override nint NumberOfSections(UITableView tableView) =>
                SearchViewController.SupportedSuggestions.Length;

            /// <param name="tableView">Table view.</param>
            /// <param name="section">Index of the section requiring a header display.</param>
            /// <summary>Called to determine the height of the header for the section specified by <paramref name="section" />.</summary>
            /// <returns>The height of the header (in points) as a <see langword="float" />.</returns>
            /// <remarks>
            ///     <para>
            ///         This method allows section headers to have different heights. This method is not called if the table is
            ///         <see cref="F:UIKit.UITableViewStyle.Plain" /> style.
            ///     </para>
            ///     <para>Declared in [UITableViewDelegate]</para>
            /// </remarks>
            public override nfloat GetHeightForHeader(UITableView tableView, nint section) =>
                SuggestionsTableSource.HeaderHeight;
            //  RowsInSection(tableView, section) == 0 ? (nfloat) SuggestionsTableSource.HeaderSpacing * 2 : SuggestionsTableSource.HeaderHeight;

            /// <param name="tableView">Table view containing the section.</param>
            /// <param name="section">Section index where the header will be added.</param>
            /// <summary>Returns a view object to display at the start of the given section.</summary>
            /// <returns>A view to be displayed at the start of the given <paramref name="section" />.</returns>
            /// <remarks>
            ///     <para>
            ///         Can either be a <see cref="T:UIKit.UILabel" />, <see cref="T:UIKit.UIImageView" /> or a custom view. This
            ///         method requires <see cref="M:UIKit.UITableViewSource.GetHeightForHeader(System.Int32,UIKit.UITableView)" /> to
            ///         be implemented as well.
            ///     </para>
            ///     <para>Declared in [UITableViewDelegate]</para>
            /// </remarks>
            public override UIView GetViewForHeader(UITableView tableView, nint section)
            {
                Suggestion suggestion = SearchViewController.SupportedSuggestions[section];
                const double spacing = SuggestionsTableSource.HeaderSpacing;
                double headerHeight = GetHeightForHeader(tableView, section);

                // if there are no rows in the section we don't display a header
                if (RowsInSection(tableView, section) == 0)
                {
                    // return new UIView(new CGRect(0, 0, tableView.Frame.Width, headerHeight));
                }

                var header = new UIView(new CGRect(0, 0, tableView.Frame.Width, headerHeight));

                var label = new UILabel
                {
                    Font = SuggestionsTableSource.HeaderFont,
                    TextColor = SuggestionsTableSource.HeaderLabelTextColor,
                    Text = SuggestionsTableSource.HeaderLabelText[suggestion]
                };

                label.SizeToFit();
                label.Frame = new CGRect(spacing, spacing, label.Frame.Width, label.Frame.Height);

                var button = new UIButton(UIButtonType.System)
                {
                    Font = SuggestionsTableSource.HeaderFont
                };
                button.SetTitleColor(SuggestionsTableSource.HeaderButtonColor[suggestion], UIControlState.Normal);
                button.SetTitle(SuggestionsTableSource.HeaderButtonText[suggestion], UIControlState.Normal);
                EventHandler buttonHandler = Superview.GetHeaderButtonHandler(suggestion);
                button.TouchUpInside += buttonHandler;

                button.SizeToFit();
                double buttonWidth = button.Frame.Width;
                button.Frame = new CGRect(tableView.Frame.Width - buttonWidth - spacing,
                                          spacing / 2,
                                          button.Frame.Width,
                                          button.Frame.Height);

                header.Add(label);
                header.Add(button);

                return header;
            }

            /// <param name="tableView">Table view containing the row.</param>
            /// <param name="indexPath">Location of the row that has become selected.</param>
            /// <summary>Called when the row specified by <paramref name="indexPath" /> is selected.</summary>
            /// <remarks>
            ///     <para>
            ///         This method can be used to perform any processing required when a row is selected, such as displaying a
            ///         <see cref="F:UIKit.UITableViewCellAccessory.Checkmark" /> accessory. In such an example, use the
            ///         <see cref="M:UIKit.UITableViewSource.RowDeselected(UIKit.UITableView,Foundation.NSIndexPath)" /> to hide the
            ///         checkmark.
            ///     </para>
            ///     <para>
            ///         Alternatively this method may push a new view controller onto a UINavigationController if the table view is
            ///         part of an hierarchical menu, or display another view or alert depending on the application's requirements.
            ///     </para>
            ///     <para>
            ///         This method is not called when the table is in editing mode (ie. when
            ///         <see cref="P:UIKit.UITableView.Editing" /> is <see langword="true" />).
            ///     </para>
            ///     <para>Declared in [UITableViewDelegate]</para>
            /// </remarks>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                // get the correct product list
                Suggestion suggestion = SearchViewController.SupportedSuggestions[indexPath.Section];
                IReadOnlyList<Product> products = GetProductList(suggestion);

                // display the selected product
                Product product = products[indexPath.Row];
                Superview.DisplayProduct(product);
            }

            private IReadOnlyList<Product> GetProductList(Suggestion suggestion) =>
                new Dictionary<Suggestion, IReadOnlyList<Product>>
                {
                    [Suggestion.RecentSearch] = this.GetCurrentUser().RecentSearches,
                    [Suggestion.Recommendation] = this.GetCurrentUser().Recommendations
                }[suggestion];
        }
    }
}