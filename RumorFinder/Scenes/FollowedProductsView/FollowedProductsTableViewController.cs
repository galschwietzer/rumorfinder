﻿using System;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Extensions.RumorFinder.Foundation;
using RumorFinder.Extensions.System;
using RumorFinder.Extensions.UIKit;
using RumorFinder.Foundation;
using RumorFinder.Foundation.Util.Event_Arguments;
using RumorFinder.Util;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{
    public partial class FollowedProductsTableViewController : UITableViewController
    {
        #region Static Fields and Constants

        private const string ErrorAlertTitle = "Cannot Update";

        private const string ErrorAlertMessage =
            "Could not perform the search, please check your internet connection and try again later.";

        #endregion

        #region Fields

        private CancellationTokenSource _currentOperation;

        private UIRefreshControl _refreshControl;

        #endregion

        #region Properties

        private ProductList FollowedProducts => this.GetCurrentUser().FollowedProducts;

        #endregion

        #region Constructors

        public FollowedProductsTableViewController(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     Called when the followed products list is changed in order to update the UI.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshTable(object sender, NotifyCollectionChangedEventArgs e)
        {
            // this method can be called on thread pool threads
            InvokeOnMainThread(() =>
            {
                switch (e)
                {
                    case CollectionChangedItemAddedArgs args:
                        var newItemIndexPath = NSIndexPath.FromRowSection(args.NewItemIndex, 0);
                        TableView.InsertRows(newItemIndexPath.AsOneObjectArray(), UITableViewRowAnimation.Top);
                        break;
                    case CollectionChangedItemRemovedArgs args:
                        var removedItemIndexPath = NSIndexPath.FromRowSection(args.RemovedItemIndex, 0);
                        TableView.DeleteRows(removedItemIndexPath.AsOneObjectArray(), UITableViewRowAnimation.Fade);
                        break;
                    case CollectionChangedItemsMovedArgs args:
                        TableView.PerformBatchUpdates(() =>
                                                      {
                                                          foreach (var item in args.Indexes)
                                                          {
                                                              var oldIndexPath =
                                                                  NSIndexPath.FromRowSection(item.oldIndex, 0);
                                                              var newIndexPath =
                                                                  NSIndexPath.FromRowSection(item.newIndex, 0);
                                                              TableView.MoveRow(oldIndexPath, newIndexPath);
                                                          }
                                                      },
                                                      null);
                        break;
                    case CollectionChangedResetArgs args:
                        NSIndexPath[] deletedIndexPaths = args.DeletedIndexes.GetIndexPaths();
                        TableView.DeleteRows(deletedIndexPaths, UITableViewRowAnimation.Fade);
                        break;
                }
            });
        }

        /// <summary>
        ///     Event listener for the pull to refresh action.
        /// </summary>
        private async Task RefreshView()
        {
            _currentOperation = new CancellationTokenSource();
            CancellationToken token = _currentOperation.Token;

            var progress = new ProgressViewReporter(ProgressBar);

            // update the list and send notifications
            try
            {
                OperationStatus status =
                    await FollowedProducts.UpdateAllProductsAsync(true, token, progress)
                                          .ConfigureAwait(false);

                if (status is OperationStatus.Failed)
                {
                    // display an alert if the update failed
                    InvokeOnMainThread(() => DisplayErrorAlert(StopRefreshing));
                }
                else
                {
                    // make sure to stop refreshing
                    InvokeOnMainThread(StopRefreshing);
                }
            }
            catch (OperationCanceledException)
            {
                InvokeOnMainThread(StopRefreshing);
            }
            finally
            {
                _currentOperation = null;
            }

            void StopRefreshing()
            {
                RefreshControl.EndRefreshingAnimated();

                // reset the progress bar
                ProgressBar.Progress = 0;
            }
        }

        /// <summary>
        ///     Displays an error alert after the list update errored out
        /// </summary>
        /// <param name="completion">a completion action to be performed after the user dismisses the alert</param>
        private void DisplayErrorAlert(Action completion = null)
        {
            // configure the alert
            var alert = UIAlertController.Create(FollowedProductsTableViewController.ErrorAlertTitle,
                                                 FollowedProductsTableViewController.ErrorAlertMessage,
                                                 UIAlertControllerStyle.Alert);
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, action => completion?.Invoke()));

            // present the alert to the user
            PresentViewController(alert, true, null);
        }

        #region View Controllers Methods

        /// <summary>Called after the controller’s <see cref="P:UIKit.UIViewController.View" /> is loaded into memory.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after <c>this</c> <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> and its entire view hierarchy have been loaded into memory. This
        ///         method is called whether the <see cref="T:UIKit.UIView" /> was loaded from a .xib file or programmatically.
        ///     </para>
        /// </remarks>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // enable large titles
            NavigationItem.LargeTitleDisplayMode = UINavigationItemLargeTitleDisplayMode.Always;
            if (NavigationController != null) NavigationController.NavigationBar.PrefersLargeTitles = true;

            // enable pull to refresh
            _refreshControl = new UIRefreshControl {TintColor = UIColor.White};
            _refreshControl.ValueChanged += async (sender, args) => await this.InvokeBackgroundSafeOperation(RefreshView);
            RefreshControl = _refreshControl;

            // doing this because it stops a visual bug from occuring
            ExtendedLayoutIncludesOpaqueBars = true;

            // configure the table view
            TableView.Source = new FollowedProductsTableViewSource {DisplayFollowedProduct = DisplayFollowedProduct};

            // allow the current followed products to appear
            TableView.ReloadData();

            // make sure to refresh the table view whenever there's a change
            FollowedProducts.CollectionChanged += RefreshTable;
        }

        /// <summary>
        ///     Called prior to the <see cref="P:UIKit.UIViewController.View" /> being added to the view hierarchy.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is called prior to the <see cref="T:UIKit.UIView" /> that is this
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property being added to
        ///         the display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewWillAppear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </remarks>
        /// <param name="animated">
        ///     <para>If the appearance will be animated.</para>
        /// </param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // ensure the progress bar is reset and that the refresh icon is not spinning
            ProgressBar.Progress = 0;
            if (_refreshControl.Refreshing)
                _refreshControl.EndRefreshingAnimated();

            // reload the table
            TableView.ReloadData();
        }

        /// <param name="animated">
        ///     <para>
        ///         <see langword="true" /> if the disappearance was animated.
        ///     </para>
        /// </param>
        /// <summary>
        ///     <para>
        ///         This method is called after the <see cref="T:UIKit.UIView" />that is <c>this</c>
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property is removed from
        ///         the display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewDidDisappear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </summary>
        /// <remarks></remarks>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            // cancel any ongoing operation
            _currentOperation?.Cancel();
        }

        #endregion
    }
}