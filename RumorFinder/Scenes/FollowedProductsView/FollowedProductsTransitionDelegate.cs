﻿using CoreGraphics;
using Foundation;
using RumorFinder.Animations;
using RumorFinder.Assets;
using RumorFinder.Extensions.Assets;
using RumorFinder.Extensions.System;
using RumorFinder.Foundation;
using RumorFinder.Scenes.Shared.ProductView;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{
    public partial class FollowedProductsTableViewController : IUIViewControllerTransitioningDelegate
    {
        #region Static Fields and Constants

        private const double TransitionDuration = 0.2;

        #endregion

        #region Fields

        private UITableViewCell _lastSelectedCell;
        private CGRect _lastSelectedCellFrame;

        #endregion

        /// <summary>
        ///     Presents a <see cref="ProductViewController"/> which displays the given product with an expanding animation
        /// </summary>
        /// <param name="followedProduct">the product to display in a <see cref="ProductViewController"/></param>
        private void DisplayFollowedProduct(Product followedProduct)
        {
            // save the selected cell
            _lastSelectedCell = TableView.CellAt(TableView.IndexPathForSelectedRow);
            _lastSelectedCellFrame = NavigationController.View.ConvertRectFromView(_lastSelectedCell.Frame, TableView);

            // initiate the view
            var productView = Storyboards.ProductView.InstantiateViewController<ProductViewController>();

            // set the followed product
            productView.SetDisplayedProduct(followedProduct);

            // initiate the view's navigation controller
            var navigationController = Storyboards
                                       .FollowedProductsView
                                       .InstantiateViewController<FollowedProductsNavigationController>();
            navigationController.NavigationBar.PrefersLargeTitles = true;
            navigationController.SetViewControllers(productView.AsOneObjectArray(), false);

            // prepare the controller to be animated
            navigationController.TransitioningDelegate = this;
            navigationController.ModalPresentationStyle = UIModalPresentationStyle.Custom;

            // present the controller
            NavigationController.PresentViewController(navigationController, true, null);
        }



        [Export("animationControllerForPresentedController:presentingController:sourceController:")]
        public IUIViewControllerAnimatedTransitioning GetAnimationControllerForPresentedController(
            UIViewController presented,
            UIViewController presenting,
            UIViewController source) => new AnimatedExpandingTransition(_lastSelectedCell, _lastSelectedCellFrame,
                                                                        FollowedProductsTableViewController
                                                                            .TransitionDuration,
                                                                        true);

        [Export("animationControllerForDismissedController:")]
        public IUIViewControllerAnimatedTransitioning GetAnimationControllerForDismissedController(
            UIViewController dismissed) =>
            new AnimatedExpandingTransition(_lastSelectedCell, _lastSelectedCellFrame,
                                            FollowedProductsTableViewController.TransitionDuration,
                                            false);
    }
}