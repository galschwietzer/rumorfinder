﻿using System;
using Foundation;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{

    /// <summary>
    ///     represents a table view source for the followed products table view.
    /// </summary>
    public class FollowedProductsTableViewSource : UITableViewSource
    {

        #region Static Fields and Constants

        private static readonly string CellIdentifier = typeof(FollowedProductTableCell).Name;

        #endregion

        #region Properties

        public Action<Product> DisplayFollowedProduct { get; set; }

        private ProductList FollowedProducts => this.GetCurrentUser().FollowedProducts;

        #endregion

        /// <summary>Called by the table view to get populate the row at <paramref name="indexPath" /> with a cell view.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called once for each row that is visible on screen. During scrolling, it is called additional
        ///         times as new rows come into view. Cells that disappear from view are cached by the table view. The
        ///         implementation of this method should call the table view's
        ///         <see cref="M:UIKit.UITableView.DequeueReusableCell(Foundation.NSString)" /> method to obtain a cached cell
        ///         object for reuse (if <see langword="null" /> is returned, create a new cell instance). Be sure to reset all
        ///         properties of a reused cell.
        ///     </para>
        ///     <para>Declared in [UITableViewDataSource]</para>
        /// </remarks>
        /// <param name="tableView">Table view requesting the cell.</param>
        /// <param name="indexPath">Location of the row where the cell will be displayed.</param>
        /// <returns>
        ///     An object that inherits from <see cref="T:UIKit.UITableViewCell" /> that the table can use for the specified
        ///     row. Do not return <see langword="null" /> or an assertion will be raised.
        /// </returns>
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            // dequeue a reusable cell
            var cell =
                tableView.DequeueReusableCell(FollowedProductsTableViewSource.CellIdentifier) as
                    FollowedProductTableCell;

            // set its attribute 
            Product followedProduct = FollowedProducts[indexPath.Row];
            cell?.SetDisplayedProduct(followedProduct);

            return cell;
        }

        /// <summary>
        ///     Called by the table view to find out how many rows are to be rendered in the section specified by
        ///     <paramref name="section" />.
        /// </summary>
        /// <remarks>
        ///     <para>Declared in [UITableViewDataSource]</para>
        /// </remarks>
        /// <param name="tableview">Table view displaying the rows.</param>
        /// <param name="section">Index of the section containing the rows.</param>
        /// <returns>Number of rows in the section at index <paramref name="section" />.</returns>
        public override nint RowsInSection(UITableView tableview, nint section) => FollowedProducts.Count;

        /// <summary>Called when the row specified by <paramref name="indexPath" /> is selected.</summary>
        /// <remarks>
        ///     <para>
        ///         This method can be used to perform any processing required when a row is selected, such as displaying a
        ///         <see cref="F:UIKit.UITableViewCellAccessory.Checkmark" /> accessory. In such an example, use the
        ///         <see cref="M:UIKit.UITableViewSource.RowDeselected(UIKit.UITableView,Foundation.NSIndexPath)" /> to hide the
        ///         checkmark.
        ///     </para>
        ///     <para>
        ///         Alternatively this method may push a new view controller onto a UINavigationController if the table view is
        ///         part of an hierarchical menu, or display another view or alert depending on the application's requirements.
        ///     </para>
        ///     <para>
        ///         This method is not called when the table is in editing mode (ie. when
        ///         <see cref="P:UIKit.UITableView.Editing" /> is <see langword="true" />).
        ///     </para>
        ///     <para>Declared in [UITableViewDelegate]</para>
        /// </remarks>
        /// <param name="tableView">Table view containing the row.</param>
        /// <param name="indexPath">Location of the row that has become selected.</param>
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            Product selectedProduct = FollowedProducts[indexPath.Row];

            DisplayFollowedProduct(selectedProduct);
        }

    }

}