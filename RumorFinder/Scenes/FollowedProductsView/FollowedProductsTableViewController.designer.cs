﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{
    [Register ("FollowedProductsTableViewController")]
    partial class FollowedProductsTableViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView MyProductsTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView ProgressBar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (MyProductsTableView != null) {
                MyProductsTableView.Dispose ();
                MyProductsTableView = null;
            }

            if (ProgressBar != null) {
                ProgressBar.Dispose ();
                ProgressBar = null;
            }
        }
    }
}