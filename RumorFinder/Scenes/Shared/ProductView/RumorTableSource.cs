﻿using System;
using System.Linq;
using Foundation;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Scenes.Shared.ProductView
{

    internal class RumorTableSource : UITableViewSource
    {

        #region Static Fields and Constants

        private static readonly string CellIdentifier = typeof(RumorTableCell).Name;

        #endregion

        #region Properties

        public Rumors DisplayedRumors { get; set; }

        public RumorCategory[] SupportedCategories { get; set; }

        public RumorCategory[] PresentedCategories =>
            SupportedCategories.Where(category => DisplayedRumors.Contains(category)).ToArray();

        #endregion

        #region Constructors

        public RumorTableSource() => DisplayedRumors = Rumors.Empty;

        #endregion

        /// <param name="tableView">Table view requesting the cell.</param>
        /// <param name="indexPath">Location of the row where the cell will be displayed.</param>
        /// <summary>Called by the table view to get populate the row at <paramref name="indexPath" /> with a cell view.</summary>
        /// <returns>
        ///     An object that inherits from <see cref="T:UIKit.UITableViewCell" /> that the table can use for the specified
        ///     row. Do not return <see langword="null" /> or an assertion will be raised.
        /// </returns>
        /// <remarks>
        ///     <para>
        ///         This method is called once for each row that is visible on screen. During scrolling, it is called additional
        ///         times as new rows come into view. Cells that disappear from view are cached by the table view. The
        ///         implementation of this method should call the table view's
        ///         <see cref="M:UIKit.UITableView.DequeueReusableCell(Foundation.NSString)" /> method to obtain a cached cell
        ///         object for reuse (if <see langword="null" /> is returned, create a new cell instance). Be sure to reset all
        ///         properties of a reused cell.
        ///     </para>
        ///     <para>Declared in [UITableViewDataSource]</para>
        /// </remarks>
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            //dequeue the rumor cell
            var rumorCell = tableView.DequeueReusableCell(RumorTableSource.CellIdentifier) as RumorTableCell;

            //select the appropriate rumor
            RumorCategory displayedCategory = PresentedCategories[indexPath.Row];
            Rumor displayedRumor = DisplayedRumors[displayedCategory];

            rumorCell?.SetDisplayedRumor(displayedRumor);

            return rumorCell;
        }

        /// <param name="tableview">Table view displaying the rows.</param>
        /// <param name="section">Index of the section containing the rows.</param>
        /// <summary>
        ///     Called by the table view to find out how many rows are to be rendered in the section specified by
        ///     <paramref name="section" />.
        /// </summary>
        /// <returns>Number of rows in the section at index <paramref name="section" />.</returns>
        /// <remarks>
        ///     <para>Declared in [UITableViewDataSource]</para>
        /// </remarks>
        public override nint RowsInSection(UITableView tableview, nint section) =>
            PresentedCategories.Length;

    }

}