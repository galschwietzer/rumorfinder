﻿using System;
using Foundation;
using RumorFinder.Assets;
using RumorFinder.Extensions.RumorFinder.Foundation;
using RumorFinder.Extensions.UIKit;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Scenes.Shared.ProductView
{

    public partial class RumorTableCell : UITableViewCell
    {

        #region Static Fields and Constants

        private const string GeneralCategoryTitle = "At A Glance";
        private const string ReleaseDateCategoryTitle = "Release Date";

        #endregion

        #region Properties

        private static UIFont ContentFont => UIFont.SystemFontOfSize(15f);
        private static UIFont UpdatesFont => UIFont.BoldSystemFontOfSize(15f);

        #endregion

        #region Constructors

        public RumorTableCell(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     method to set the displayed rumor of this table cell
        /// </summary>
        /// <param name="newDisplayedRumor"></param>
        public void SetDisplayedRumor(Rumor newDisplayedRumor)
        {
            RumorCategory newCategory = newDisplayedRumor.Category;

            //set the correct title
            CategoryTitle.Text = TitleFor(newCategory);

            //set the correct icon
            UIImage categoryIcon = Icons.For(newCategory);
            CategoryIcon.SetImage(categoryIcon);

            //set the correct text
            var contentAttributes = new UIStringAttributes {Font = RumorTableCell.ContentFont};
            var updatesAttributes = new UIStringAttributes {Font = RumorTableCell.UpdatesFont};
            NSAttributedString text = newDisplayedRumor.ToAttributedString(contentAttributes, updatesAttributes);
            Text.AttributedText = text;

            //returns the appropriate title for the given category
            string TitleFor(RumorCategory category)
            {
                switch (category)
                {
                    case RumorCategory.General:
                        return RumorTableCell.GeneralCategoryTitle;
                    case RumorCategory.ReleaseDate:
                        return RumorTableCell.ReleaseDateCategoryTitle;
                    default:
                        return category.ToString();
                }
            }
        }

    }

}