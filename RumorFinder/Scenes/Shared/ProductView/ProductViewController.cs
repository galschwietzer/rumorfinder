﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoreGraphics;
using RumorFinder.Assets;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Extensions.System;
using RumorFinder.Extensions.UIKit;
using RumorFinder.Foundation;
using RumorFinder.Util;
using UIKit;

namespace RumorFinder.Scenes.Shared.ProductView
{
    public partial class ProductViewController : UITableViewController
    {
        #region Static Fields and Constants

        private const double ErrorLabelHeight = 78;
        private const double ErrorDisplaySpacing = 20;

        private const string UnsuccessfulSearchMessageFormat = "No Rumors could be found for {0}";

        private const string FailedSearchMessage =
            "Could not perform the search, please check your internet connection and try again later.";

        private static readonly RumorCategory[] SupportedCategories =
        {
            RumorCategory.Features,
            RumorCategory.Design,
            RumorCategory.Specifications,
            RumorCategory.Price,
            RumorCategory.ReleaseDate
        };

        private static readonly UIFont ErrorMessageFont = UIFont.SystemFontOfSize(18f);

        #endregion

        #region Fields

        private CancellationTokenSource _currentSearch;
        private Task<OperationStatus> _currentSearchTask;

        private bool _errorIsDisplayed;

        private FollowButton _followButton;

        #endregion

        #region Properties

        private User CurrentUser => this.GetCurrentUser();

        private IEnumerable<RumorCategory> PresentedCategories =>
            ProductViewController.SupportedCategories
                                 .Where(category =>
                                            DisplayedProduct?.Rumors.Contains(category) is true);

        public Product DisplayedProduct { get; private set; }

        public UIImageView ErrorIcon { get; private set; }

        public UILabel ErrorMessageLabel { get; private set; }

        #endregion

        #region Constructors

        public ProductViewController(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     method to set the currently displayed product
        /// </summary>
        /// <remarks>
        ///     this method should only be called before the view controller is presented
        /// </remarks>
        /// <param name="newDisplayedProduct">The product to now be displayed in the view</param>
        public void SetDisplayedProduct(Product newDisplayedProduct)
        {
            // remove the previously displayed rumors from the screen(if there were)
            var rumorTableSource = TableView.Source as RumorTableSource;
            if (!rumorTableSource.DisplayedRumors.IsEmpty)
            {
                rumorTableSource.DisplayedRumors = Rumors.Empty;
                TableView.ReloadData();
            }

            // hide any error that is displayed
            if (_errorIsDisplayed)
            {
                ErrorIcon.RemoveFromSuperview();
                ErrorMessageLabel.RemoveFromSuperview();
                _errorIsDisplayed = false;
            }

            // configure the screen according to the new product
            DisplayedProduct = newDisplayedProduct;

            Title = DisplayedProduct.Name;

            // display the progress bar
            ProgressBar.Hidden = false;

            if (CurrentUser.Follows(DisplayedProduct))
            {
                // enable and highlight the follow button
                _followButton.UpdateButtonForState(UIControlState.Selected);

                // if we follow the product there's no need to search fro rumors again
                _currentSearchTask = Task.FromResult(OperationStatus.Successful);
            }
            else
            {
                // disable the follow button
                _followButton.UpdateButtonForState(UIControlState.Disabled);

                // start the rumor search
                _currentSearchTask = this.InvokeBackgroundSafeOperation(FindNewRumorsAsync);
            }
        }

        /// <summary>
        ///     event listener for when the follow button gets clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FollowButtonClicked(object sender, EventArgs e)
        {
            UIControlState updatedState;

            // if we follow the product, we un-follow else we follow the product
            if (CurrentUser.Follows(DisplayedProduct))
            {
                CurrentUser.UnFollow(DisplayedProduct);
                updatedState = UIControlState.Normal;
            }
            else
            {
                CurrentUser.Follow(DisplayedProduct);
                updatedState = UIControlState.Selected;
            }

            // update the button to reflect the changes
            _followButton.UpdateButtonForState(updatedState);
        }

        /// <summary>
        ///     finds new rumors for newly searched products.
        /// </summary>
        /// <returns>the status of the search operation</returns>
        private async Task<OperationStatus> FindNewRumorsAsync()
        {
            _currentSearch = new CancellationTokenSource();
            CancellationToken token = _currentSearch.Token;

            var progress = new ProgressViewReporter(ProgressBar);

            return await DisplayedProduct.FindRumorsAsync(true, token, progress).ConfigureAwait(false);
        }

        /// <summary>
        ///     Awaits the current search task to completion and update the ui with the results
        /// </summary>
        /// <returns>The task that represents this operation.</returns>
        private async Task CompleteCurrentTaskAndUpdateUi()
        {
            OperationStatus searchStatus;
            try
            {
                searchStatus = await _currentSearchTask.ConfigureAwait(false);
            }
            catch (OperationCanceledException)
            {
                //the search was canceled
                return;
            }
            finally
            {
                //reset the progress bar
                InvokeOnMainThread(() => ProgressBar.Hidden = true);

                _currentSearch = null;
            }

            InvokeOnMainThread(UpdateUi);

            // update the UI to reflect the changes
            void UpdateUi()
            {
                // update the follow button's appearance
                if (CurrentUser.Follows(DisplayedProduct))
                    _followButton.UpdateButtonForState(UIControlState.Selected);
                else
                {
                    // the update button is only enabled if the search hasn't failed
                    _followButton.UpdateButtonForState(searchStatus is OperationStatus.Failed
                                                           ? UIControlState.Disabled
                                                           : UIControlState.Normal);
                }

                //handle the search's success or failure
                if (searchStatus is OperationStatus.Successful)
                {
                    // update the table by inserting the content into view
                    var rumorTableSource = TableView.Source as RumorTableSource;
                    rumorTableSource.DisplayedRumors = DisplayedProduct.Rumors;

                    // use the correct animation according to the table view's visible cells
                    if (TableView.VisibleCells.Length > 0)
                    {
                        TableView.ReloadData();
                    }
                    else
                    {
                        TableView.InsertRows(PresentedCategories.GetIndexPaths(),
                                             UITableViewRowAnimation.Top);
                    }
                }
                else
                {
                    UIImage errorIcon;
                    string errorMessage;
                    if (searchStatus == OperationStatus.Failed)
                    {
                        errorIcon = Icons.FailedConnection;
                        errorMessage = ProductViewController.FailedSearchMessage;
                    }
                    else
                    {
                        errorIcon = Icons.SearchUnsuccessful;
                        errorMessage = string.Format(ProductViewController.UnsuccessfulSearchMessageFormat,
                                                     DisplayedProduct.Name);
                    }

                    //display the error to the user
                    DisplayError(errorIcon, errorMessage);
                }
            }
        }

        #region View Controller Overrides

        /// <summary>Called after the controller’s <see cref="P:UIKit.UIViewController.View" /> is loaded into memory.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after <c>this</c> <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> and its entire view hierarchy have been loaded into memory. This
        ///         method is called whether the <see cref="T:UIKit.UIView" /> was loaded from a .xib file or programmatically.
        ///     </para>
        /// </remarks>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // enable large titles
            NavigationItem.LargeTitleDisplayMode = UINavigationItemLargeTitleDisplayMode.Always;

            // create and assign the back bar button
            var backButton = new UIBarButtonItem {Image = Icons.BackButton};
            backButton.Clicked +=
                (sender, e) =>
                {
                    if (NavigationController?.ViewControllers.Length > 1)
                    {
                        NavigationController?.PopViewController(true);
                    }
                    else
                    {
                        NavigationController?.DismissViewController(true, null);
                    }

                };
            NavigationItem.LeftBarButtonItem = backButton;

            // configure the follow button
            _followButton = new FollowButton();
            _followButton.ButtonView.TouchUpInside += FollowButtonClicked;
            var followBarButton = new UIBarButtonItem
            {
                CustomView = _followButton.ButtonView,
                PossibleTitles = _followButton.PossibleTitles
            };
            NavigationItem.RightBarButtonItem = followBarButton;

            // configure the TableView
            TableView.AllowsSelection = false;

            // enable auto sizing row dimensions
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 100f;

            // set up the table source
            TableView.Source = new RumorTableSource {SupportedCategories = ProductViewController.SupportedCategories};

            // set up the error display
            AddErrorDisplay();
        }

        /// <param name="animated">
        ///     <para>If the appearance will be animated.</para>
        /// </param>
        /// <summary>
        ///     Called prior to the <see cref="P:UIKit.UIViewController.View" /> being added to the view hierarchy.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is called prior to the <see cref="T:UIKit.UIView" /> that is this
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property being added to
        ///         the display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewWillAppear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </remarks>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // show the navigation bar so the title can be displayed
            // we hide and then show the navigation bar to "refresh" it and to solve visual bugs that occur. 
            NavigationController?.SetNavigationBarHidden(true, false);
            NavigationController?.SetNavigationBarHidden(false, false);
        }

        /// <param name="animated">
        ///     <para>
        ///         <see langword="true" /> if the appearance was animated.
        ///     </para>
        /// </param>
        /// <summary>
        ///     Called after the <see cref="P:UIKit.UIViewController.View" /> is added to the view hierarchy.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after the <see cref="T:UIKit.UIView" /> that is <c>this</c>
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property is added to the
        ///         display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewDidAppear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </remarks>
        public override async void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            await CompleteCurrentTaskAndUpdateUi().ConfigureAwait(false);
        }

        /// <param name="animated">
        ///   <para>If the appearance will be animated.</para>
        /// </param>
        /// <summary>
        ///   <para>This method is called prior to the removal of the <see cref="T:UIKit.UIView" />that is this <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> from the display <see cref="T:UIKit.UIView" /> hierarchy.</para>
        ///   <para>Application developers may override this method to configure animations, resign first responder status (see <see cref="M:UIKit.UIResponder.ResignFirstResponder" />), or perform other tasks.</para>
        ///   <para>Application developers who override this method must call <c>base.ViewWillDisappear()</c> in their overridden method.</para>
        /// </summary>
        /// <remarks></remarks>
        public override async void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            // if the product was updated we mark the updates as seen
            if (DisplayedProduct.Rumors.HasUpdates)
            {
                await DisplayedProduct.UpdatesSeenAsync();
            }
        }

        /// <param name="animated">
        ///     <para>
        ///         <see langword="true" /> if the disappearance was animated.
        ///     </para>
        /// </param>
        /// <summary>
        ///     <para>
        ///         This method is called after the <see cref="T:UIKit.UIView" />that is <c>this</c>
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property is removed from
        ///         the display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewDidDisappear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </summary>
        /// <remarks></remarks>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            //cancel any outgoing searches
            _currentSearch?.Cancel();

            // un-display the error views if they're onscreen
            if (_errorIsDisplayed)
            {
                ErrorIcon.RemoveFromSuperview();
                ErrorMessageLabel.RemoveFromSuperview();
                _errorIsDisplayed = false;
            }

            
        }

        #endregion

        #region Private Helpers

        /// <summary>
        ///     Adds the appropriate controls to display and error to the user
        /// </summary>
        private void AddErrorDisplay()
        {
            ErrorIcon = new UIImageView();

            //create the error message label
            ErrorMessageLabel = new UILabel
            {
                //set the default bounds
                Bounds = new CGRect(0, 0, TableView.Bounds.Width - ProductViewController.ErrorDisplaySpacing / 2, ProductViewController.ErrorLabelHeight),

                Font = ProductViewController.ErrorMessageFont,
                TextColor = Colors.Error,
                TextAlignment = UITextAlignment.Center,
                Lines = 3
            };
        }

        /// <summary>
        ///     displays an error to the user
        /// </summary>
        /// <param name="errorIcon"></param>
        /// <param name="errorMessage"></param>
        private void DisplayError(UIImage errorIcon, string errorMessage)
        {
            ErrorIcon.SetImage(errorIcon);
            ErrorIcon.TintColor = Colors.Error;

            ErrorMessageLabel.Text = errorMessage;

            //set the positions - label is set in the middle of the screen and icon is above it
            ErrorMessageLabel.Center = TableView.Center;
            ErrorIcon.Center = new CGPoint(ErrorMessageLabel.Center.X,
                                           ErrorMessageLabel.Center.Y - ErrorIcon.Bounds.Height / 2 - ProductViewController.ErrorDisplaySpacing);

            TableView?.Superview?.AddSubviews(ErrorMessageLabel, ErrorIcon);

            _errorIsDisplayed = true;
        }

        #endregion
    }
}