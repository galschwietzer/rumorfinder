﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace RumorFinder.Scenes.Shared.ProductView
{
    [Register ("RumorTableCell")]
    partial class RumorTableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView CategoryIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CategoryTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CategoryIcon != null) {
                CategoryIcon.Dispose ();
                CategoryIcon = null;
            }

            if (CategoryTitle != null) {
                CategoryTitle.Dispose ();
                CategoryTitle = null;
            }

            if (Text != null) {
                Text.Dispose ();
                Text = null;
            }
        }
    }
}