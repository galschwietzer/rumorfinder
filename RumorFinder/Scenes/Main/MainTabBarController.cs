﻿using System;
using Foundation;
using RumorFinder.Animations;
using RumorFinder.Assets;
using RumorFinder.Extensions.Assets;
using RumorFinder.Scenes.FollowedProductsView;
using RumorFinder.Scenes.HomeView;
using UIKit;

namespace RumorFinder.Scenes.Main
{

    public partial class MainTabBarController : UITabBarController, IUITabBarControllerDelegate
    {

        #region Static Fields and Constants

        private static readonly UIEdgeInsets ImageInsets = new UIEdgeInsets(6, 0, -6, 0);

        private const double TransitionDuration = 0.2;

        #endregion

        #region Constructors

        public MainTabBarController(IntPtr handle) : base(handle)
        { }

        #endregion

        #region View Controller Methods

        /// <summary>Called after the controller’s <see cref="P:UIKit.UIViewController.View" /> is loaded into memory.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after <c>this</c> <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> and its entire view hierarchy have been loaded into memory. This
        ///         method is called whether the <see cref="T:UIKit.UIView" /> was loaded from a .xib file or programmatically.
        ///     </para>
        /// </remarks>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // enable custom transitions
            Delegate = this;

            // create and assign the base view controllers
            var homeViewController = Storyboards.HomeView.InstantiateViewController<HomeViewController>();
            var followedProductsNavigationController =
                Storyboards.FollowedProductsView.InstantiateViewController<FollowedProductsNavigationController>();

            // set up tab bar icons
            homeViewController.TabBarItem = new UITabBarItem(string.Empty, Icons.Home, Icons.HomeSelected);
            followedProductsNavigationController.TabBarItem =
                new UITabBarItem(string.Empty, Icons.Products, Icons.ProductsSelected);

            SetViewControllers(new UIViewController[] {homeViewController, followedProductsNavigationController},
                               false);

            // customise the appearance of the bar
            TabBar.TintColor = UIColor.Clear;
            TabBar.SelectedImageTintColor = UIColor.Clear;
            TabBar.BarTintColor = Colors.SecondaryBackgroundColor;
            ;
            TabBar.Translucent = false;
            View.BackgroundColor = Colors.SecondaryBackgroundColor;

            // select the initial view controller
            SelectedViewController = homeViewController;

            // remove all titles from the Bar
            foreach (UITabBarItem barItem in TabBar.Items)
            {
                barItem.Title = string.Empty;
                barItem.ImageInsets = MainTabBarController.ImageInsets;
            }
        }

        #endregion

        #region Tab Bar Delegate Methods

        [Export("tabBarController:animationControllerForTransitionFromViewController:toViewController:")]
        public UIKit.IUIViewControllerAnimatedTransitioning GetAnimationControllerForTransition(
            UITabBarController tabBarController,
            UIViewController fromViewController,
            UIViewController toViewController)
        {
            UIViewController[] viewControllers = tabBarController.ViewControllers;
            int fromViewIndex = Array.IndexOf(viewControllers, fromViewController);
            int toViewIndex = Array.IndexOf(viewControllers, toViewController);

            return new AnimatedHorizontalTransition(MainTabBarController.TransitionDuration, toViewIndex < fromViewIndex);
        }

        #endregion

    }

}